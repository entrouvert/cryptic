/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <glib-object.h>

#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>
#include <openssl/dsa.h>
#include <openssl/evp.h>
#include <openssl/crypto.h>
#include <openssl/bio.h>
#include <openssl/rand.h>
#include <openssl/err.h>

#include "../cryptic/cryptic.h"
#include "../cryptic/utils.h"

#ifdef OPENSSL_SYS_WIN16
#define MS_CALLBACK     _far _loadds
#else
#define MS_CALLBACK
#endif

static int MS_CALLBACK qrn_cb(int p, int n, BN_GENCB *arg);
static BIO *bio_err=NULL;

BN_GENCB cb;

void activ_CB(){
	if (bio_err == NULL) bio_err=BIO_new_fp(stderr,BIO_NOCLOSE);
	BN_GENCB_set(&cb, qrn_cb, bio_err);
	BN_GENCB_call(&cb, 2, 0);
	printf("Callback function activated.\n\n");
}

int test_1(){
	int rc = CRYPTIC_ERROR_UNDEFINED;

	printf("\n");
	printf("------******------------------------------------------------------------------******------\n");
	printf("------******---------------------- Certificate generation --------------------******------\n");
	printf("------******-------------------------------- & -------------------------------******------\n");
	printf("------******------------------------ Parameters loading ----------------------******------\n");
	printf("------******------------------------------------------------------------------******------\n\n");

	int i;
	CrypticCommitDataStore *pdc;
	CrypticClsig *param1, *param2, *param3, *param4;
	BIGNUM *bases[2], *quantities[2], **tmp;
	pdc = NULL;
	param1 = NULL;
	param2 = NULL;
	param3 = NULL;
	param4 = NULL;
	bases[0] = NULL;
	bases[1] = NULL;
	quantities[0] = NULL;
	quantities[1] = NULL;

	for(i=0; i<2; i++){
		goto_cleanup_if_fail_with_rc_with_warning_openssl(bases[i] = BN_new());
		goto_cleanup_if_fail_with_rc_with_warning_openssl(quantities[i] = BN_new());
	}

	printf("*** 1. Issuer1\n");
	printf("\n----> \tParameters generation\n");

	param1 = cryptic_clsig_new(1024, 80, 298, 0, 0, 0, 5);
	goto_cleanup_if_fail_with_warning(param1 != NULL);

	//goto_cleanup_if_fail_with_rc_with_warning_openssl(cryptic_clsig_generate_parameters(param1,&cb) == 0);
	cryptic_check_good_rc(cryptic_clsig_generate_parameters(param1));

	printf("\n----> \tCertificate generation\n");

	cryptic_check_good_rc(cryptic_clsig_compute_dlrep_with_random_quantities(param1,5));
	cryptic_check_good_rc(cryptic_clsig_sign(param1));
	if (cryptic_clsig_verify_signature_not_randomized(param1)) {
		printf("\t****** VALID SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad signature\n");
		return(-1);
	}




	printf("\n*** 2. Prover1\n\n");
	printf("----> \tParameters loading\n");
	param2 = cryptic_clsig_new_load_public_parameters(cryptic_clsig_get_Z(param1),
				cryptic_clsig_get_S(param1),
				cryptic_clsig_get_nb_bases(param1),
				cryptic_clsig_get_bases(param1),
				cryptic_clsig_get_lg_quantities(param1),
				cryptic_clsig_get_lg_exponent(param1),
				cryptic_clsig_get_modulus(param1),
				cryptic_clsig_get_lg_sec_param(param1),
				cryptic_clsig_get_lg_zk_sec_param(param1),
				cryptic_clsig_get_lg_clsig_sec_param(param1));
	goto_cleanup_if_fail_with_warning(param2 != NULL);

	printf("----> \tCertificate loading\n");
	if (cryptic_clsig_load_certificate(param2,
				cryptic_clsig_get_signature(param1),
				cryptic_clsig_get_exponent(param1),
				cryptic_clsig_get_blind(param1),
				cryptic_clsig_get_quantities(param1), cryptic_clsig_get_nb_quantities(param1),
				1) == 1 ){
		printf("\t****** VALID SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad signature\n");
		return(-1);
	}

	goto_cleanup_if_fail_with_rc_with_warning_openssl(cryptic_clsig_randomize_signature(param2) == 0);
	if ( cryptic_clsig_verify_signature_randomized(param2) == 1 ){
		printf("\t****** VALID Randomized SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad Randomized signature\n");
		return(-1);
	}




	printf("\n*** 1. Prover2\n\n");
	printf("----> \tParameters loading\n");
	param3 = cryptic_clsig_new_load_public_parameters(cryptic_clsig_get_Z(param1),
				cryptic_clsig_get_S(param1),
				cryptic_clsig_get_nb_bases(param1),
				cryptic_clsig_get_bases(param1),
				cryptic_clsig_get_lg_quantities(param1),
				cryptic_clsig_get_lg_exponent(param1),
				cryptic_clsig_get_modulus(param1),
				cryptic_clsig_get_lg_sec_param(param1),
				cryptic_clsig_get_lg_zk_sec_param(param1),
				cryptic_clsig_get_lg_clsig_sec_param(param1));
	goto_cleanup_if_fail_with_warning(param3 != NULL);

	printf("----> \tGenerate Commitment\n");
	tmp = cryptic_clsig_get_bases(param3);
	goto_cleanup_if_fail_with_warning(tmp != NULL);
	for(i=0;i<2;i++){
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(bases[i],tmp[i+3]));
	}
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(quantities[0],123) == 1);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(quantities[1],456) == 1);

	pdc = cryptic_commit_data_store_new();
	goto_cleanup_if_fail_with_warning(pdc != NULL);

	goto_cleanup_if_fail_with_rc_with_warning_openssl( cryptic_clsig_compute_committed_value(param3, pdc, bases, quantities, 2) == 0);




	printf("\n*** 2. Issuer1\n\n");
	printf("----> \tCertificate generation\n");
	goto_cleanup_if_fail_with_rc_with_warning_openssl(cryptic_clsig_compute_dlrep_with_random_quantities(param1,3) == 0);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(cryptic_clsig_sign_with_committed_value(param1, pdc->dlrep) == 0);
	if (cryptic_clsig_verify_signature_not_randomized(param1)) {
		printf("\t****** VALID SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad signature\n");
		return(-1);
	}




	printf("\n*** 3. Prover2\n\n");
	printf("----> \tCertificate loading\n");
	if (cryptic_clsig_load_certificate_with_committed_value(param3,
				cryptic_clsig_get_signature(param1),
				cryptic_clsig_get_exponent(param1),
				cryptic_clsig_get_blind(param1),
				cryptic_clsig_get_quantities(param1), cryptic_clsig_get_nb_quantities(param1),
				quantities, 2,
				pdc->dlrep, pdc->vprime) == 0 ){
		printf("\t****** VALID SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad signature\n");
		return(-1);
	}
	goto_cleanup_if_fail_with_rc_with_warning_openssl(cryptic_clsig_randomize_signature(param3) == 0);
	if (cryptic_clsig_verify_signature_randomized(param3) == 1 ){
		printf("\t****** VALID Randomized SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad Randomized signature\n");
		return(-1);
	}




	/* New issuer */
	printf("\n*** 2. Issuer2\n\n");
	printf("----> \tIssuer1 parameters loading\n");
	param4 = cryptic_clsig_new_load_parameters_issuer(cryptic_clsig_get_private_composite(param1),
				cryptic_clsig_get_Z(param1),
				cryptic_clsig_get_S(param1),
				cryptic_clsig_get_nb_bases(param1),
				cryptic_clsig_get_bases(param1),
				cryptic_clsig_get_lg_quantities(param1),
				cryptic_clsig_get_lg_exponent(param1),
				cryptic_clsig_get_modulus(param1),
				cryptic_clsig_get_lg_sec_param(param1),
				cryptic_clsig_get_lg_zk_sec_param(param1),
				cryptic_clsig_get_lg_clsig_sec_param(param1));
				//cryptic_clsig_get_lg_clsig_sec_param(param1),
				//&cb);
	goto_cleanup_if_fail_with_warning(param4 != NULL);

	printf("\n----> \tCertificate generation\n");
	goto_cleanup_if_fail_with_rc_with_warning_openssl(cryptic_clsig_compute_dlrep_with_random_quantities(param4,3) == 0);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(cryptic_clsig_sign_with_committed_value(param4, pdc->dlrep) == 0);
	if (cryptic_clsig_verify_signature_not_randomized(param4)) {
		printf("\t****** VALID SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad signature\n");
		return(-1);
	}




	printf("\n*** 3. Prover2\n\n");
	printf("----> \tCertificate loading\n");
	if (cryptic_clsig_load_certificate_with_committed_value(param3,
				cryptic_clsig_get_signature(param4),
				cryptic_clsig_get_exponent(param4),
				cryptic_clsig_get_blind(param4),
				cryptic_clsig_get_quantities(param4), cryptic_clsig_get_nb_quantities(param4),
				quantities, 2,
				pdc->dlrep, pdc->vprime) == 0 ){
		printf("\t****** VALID SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad signature\n");
		return(-1);
	}
	goto_cleanup_if_fail_with_rc_with_warning_openssl(cryptic_clsig_randomize_signature(param3) == 0);
	if ( cryptic_clsig_verify_signature_randomized(param3) == 1 ){
		printf("\t****** VALID Randomized SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad Randomized signature\n");
		return(-1);
	}




	rc = CRYPTIC_NO_ERROR;
cleanup:
	for(i=0; i<2; i++){
		cryptic_release_bn(bases[i]);
		cryptic_release_bn(quantities[i]);
	}
	cryptic_release_gobject(param1);
	cryptic_release_gobject(param2);
	cryptic_release_gobject(param3);
	cryptic_release_gobject(param4);
	cryptic_release_gobject(pdc);
	return rc;
}

int test_2(){
	int rc = CRYPTIC_ERROR_UNDEFINED;

	printf("\n");
	printf("------******------------------------------------------------------------------******------\n");
	printf("------******---------------------------- Prove a CLSIG -----------------------******------\n");
	printf("------******------------------------------------------------------------------******------\n\n");


	int i, z, j;
	int nb_bases = 5;
	int nb_messages = 5;
	int *i_q, *i_b;
	i_q = NULL;
	i_b = NULL;
	char* str[5];
	for(i=0;i<5;i++){
		str[i] = NULL;
	}
	/* Issuer */
	CrypticClsig *param1 = NULL;
	/* Prover */
	CrypticClsig *param2 = NULL;
	CrypticZkpkSchnorr *shn1 = NULL;
	CrypticZkpkSchnorr *shn3 = NULL;
	CrypticProofrangeQrg *pr1 = NULL;
	CrypticProofrangeQrg *pr3 = NULL;
	CrypticHashForNiProofs* hash1 = NULL;
	/* Verifier */
	CrypticClsig *param3 = NULL;
	CrypticZkpkSchnorr *shn2 = NULL;
	CrypticZkpkSchnorr *shn4 = NULL;
	CrypticProofrangeQrg *pr2 = NULL;
	CrypticProofrangeQrg *pr4 = NULL;
	CrypticHashForNiProofs* hash2 = NULL;
	BIGNUM *m = NULL, *b = NULL, *random_m = NULL, **tmp = NULL, *s1 = NULL, **s2 = NULL, *tmp1 = NULL, *P = NULL, *challenge = NULL;
	BIGNUM *tab_gene[nb_messages+2];
	BIGNUM *tab_qty[nb_messages+2];
	for(j=0; j<nb_messages+2; j++){
		tab_gene[j] = NULL;
		tab_qty[j] = NULL;
	}
	BN_CTX *ctx = NULL;

	goto_cleanup_if_fail_with_rc_with_warning_openssl(b = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(m = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(random_m = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(tmp1 = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(challenge = BN_new());
	//goto_cleanup_if_fail_with_rc_with_warning_openssl(P = BN_new());
	for(j=0; j<nb_messages+2; j++){
		goto_cleanup_if_fail_with_rc_with_warning_openssl(tab_gene[j] = BN_new());
		goto_cleanup_if_fail_with_rc_with_warning_openssl(tab_qty[j] = BN_new());
	}

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ctx = BN_CTX_new());

	str[0]="Gui";
	printf("PRENOM:\t\t\t%s\n",str[0]);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_bin2bn((const unsigned char *) str[0],strlen(str[0]),tab_qty[0]));
	cryptic_check_good_rc(cryptic_print_bn("\t\t\t",tab_qty[0]));

	str[1]="Montag";
	printf("NOM:\t\t\t%s\n",str[1]);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_bin2bn((const unsigned char *) str[1],strlen(str[1]),tab_qty[1]));
	cryptic_check_good_rc(cryptic_print_bn("\t\t\t",tab_qty[1]));

	str[2]="1950010100";
	printf("DATE DE NAISSANCE:\t%s\n",str[2]);
	//Put an int not a string!
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(m,1982012803) == 1);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_qty[2],m));
	cryptic_check_good_rc(cryptic_print_bn("\t\t\t",tab_qty[2]));

	str[3]="unknown";
	printf("LIEU DE NAISSANCE:\t%s\n",str[3]);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_bin2bn((const unsigned char *) str[3],strlen(str[3]),tab_qty[3]));
	cryptic_check_good_rc(cryptic_print_bn("\t\t\t",tab_qty[3]));

	str[4]="Fahrenheit";
	printf("ADRESSE:\t\t%s\n",str[4]);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_bin2bn((const unsigned char *) str[4],strlen(str[4]),tab_qty[4]));
	cryptic_check_good_rc(cryptic_print_bn("\t\t\t",tab_qty[4]));




	printf("\n\n1. *** ISSUER\n");
	printf("\n----> \tParameters generation - WARNING: for the non-interactive proof lg messages >= lg hash\n");
	param1 = cryptic_clsig_new(1024, 256, 596, 0, 0, 0, 5);
	goto_cleanup_if_fail_with_warning(param1 != NULL);
	//goto_cleanup_if_fail_with_rc_with_warning_openssl(cryptic_clsig_generate_parameters(param1,&cb) == 0);
	cryptic_check_good_rc(cryptic_clsig_generate_parameters(param1));

	printf("\n----> \tCertificate generation\n");
	//cryptic_check_good_rc(cryptic_clsig_compute_dlrep_bulk_from_offset(param1,tab_qty,0,5));
	int index[5] = {0,1,2,3,4};
	cryptic_check_good_rc(cryptic_clsig_compute_dlrep_by_index(param1,tab_qty,index,5));
	cryptic_check_good_rc(cryptic_clsig_sign(param1));




	printf("\n\n2. *** PROVER\n");
	printf("----> \tParameters loading\n");
	param2 = cryptic_clsig_new_load_public_parameters(cryptic_clsig_get_Z(param1),
				cryptic_clsig_get_S(param1),
				cryptic_clsig_get_nb_bases(param1),
				cryptic_clsig_get_bases(param1),
				cryptic_clsig_get_lg_quantities(param1),
				cryptic_clsig_get_lg_exponent(param1),
				cryptic_clsig_get_modulus(param1),
				cryptic_clsig_get_lg_sec_param(param1),
				cryptic_clsig_get_lg_zk_sec_param(param1),
				cryptic_clsig_get_lg_clsig_sec_param(param1));
	goto_cleanup_if_fail_with_warning(param2 != NULL);

	printf("----> \tCertificate loading\n");
	if (cryptic_clsig_load_certificate(param2,
				cryptic_clsig_get_signature(param1),
				cryptic_clsig_get_exponent(param1),
				cryptic_clsig_get_blind(param1),
				cryptic_clsig_get_quantities(param1), cryptic_clsig_get_nb_quantities(param1),
				1) == 1 ){
		printf("\t****** VALID SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad signature\n");
		return(-1);
	}
	printf("----> \tCertificate randomization\n");
	cryptic_check_good_rc(cryptic_clsig_randomize_signature(param2));
	if ( cryptic_clsig_verify_signature_randomized(param2) == 1 ){
		printf("\t****** VALID Randomized SIGNATURE\n");
	} else {
		printf("\t!!!!!! Bad Randomized signature\n");
		return(-1);
	}




	printf("\n\n3. *** VERIFIER\n");
	printf("----> \tParameters loading\n");
	param3 = cryptic_clsig_new_load_public_parameters(cryptic_clsig_get_Z(param1),
				cryptic_clsig_get_S(param1),
				cryptic_clsig_get_nb_bases(param1),
				cryptic_clsig_get_bases(param1),
				cryptic_clsig_get_lg_quantities(param1),
				cryptic_clsig_get_lg_exponent(param1),
				cryptic_clsig_get_modulus(param1),
				cryptic_clsig_get_lg_sec_param(param1),
				cryptic_clsig_get_lg_zk_sec_param(param1),
				cryptic_clsig_get_lg_clsig_sec_param(param1));
	goto_cleanup_if_fail_with_warning(param3 != NULL);

	printf("----> \tSay: 'Prove me that you have a valid certificate from ISSUER showing your name and prove you are over 18 years old'\n");
	/* The prover must prove that the quantity (exponent) of the base 2 is inferior to b */
	/* The base two is trusted by the verifier as being the base used by the issuer to represent the birth date */
	/* Prove an age: the birth date < Today - 18 years */
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(b,1992021900) == 1);




	printf("\n\n4. *** PROVER\n");

	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_gene[0],cryptic_clsig_get_randomized_signature(param2)));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_gene[1],cryptic_clsig_get_S(param2)));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_gene[2],cryptic_clsig_get_i_base(param2,0)));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_gene[3],cryptic_clsig_get_i_base(param2,2)));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_gene[4],cryptic_clsig_get_i_base(param2,3)));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_gene[5],cryptic_clsig_get_i_base(param2,4)));

//	BN_copy(tab_qty[0],cryptic_clsig_get_exponent(param2));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_qty[0],cryptic_clsig_get_exponent_corrected(param2)));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_qty[1],cryptic_clsig_get_random_blind(param2)));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_bin2bn((const unsigned char *) str[0],strlen(str[0]),tmp1));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_qty[2],tmp1));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_qty[3],m));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_bin2bn((const unsigned char *) str[3],strlen(str[3]),tmp1));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_qty[4],tmp1));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_bin2bn((const unsigned char *) str[4],strlen(str[4]),tmp1));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_qty[5],tmp1));

	printf("----> \tTodo:\n");
	printf("----> \t1- Reveal the name\n");
	printf("----> \t2- Compute Z/base^Nom and prove it\n");
	printf("----> \t3- Prove that the quantity represented by the base 2 in the following proof is inferior to b.\n");

	/* P = Z. R1^-nom . A^-(2le-1) = A_rand^e_corrected . S^v_rand ...*/
	i_q = (int*) g_malloc(sizeof(int));
	i_b = (int*) g_malloc(sizeof(int));
	i_q[0]=1;
	i_b[0]=1;
	P = cryptic_clsig_build_dlrep_before_proving(param2, i_q, i_b, 1);	
	goto_cleanup_if_fail_with_warning(P != NULL);


	printf("----> \tProof initialization\n");
	shn1 = cryptic_zkpk_schnorr_new(tab_gene, 6, cryptic_clsig_get_modulus(param2));
	goto_cleanup_if_fail_with_warning(shn1 != NULL);
	shn3 = cryptic_zkpk_schnorr_new(tab_gene, 6, cryptic_clsig_get_modulus(param2));
	goto_cleanup_if_fail_with_warning(shn3 != NULL);
	pr1 = cryptic_proofrange_qrg_new(cryptic_clsig_get_Z(param2),cryptic_clsig_get_S(param2),cryptic_clsig_get_modulus(param2));
	goto_cleanup_if_fail_with_warning(pr1 != NULL);
	pr3 = cryptic_proofrange_qrg_new(cryptic_clsig_get_Z(param2),cryptic_clsig_get_S(param2),cryptic_clsig_get_modulus(param2));
	goto_cleanup_if_fail_with_warning(pr3 != NULL);

	printf("----> \tProof first round: Commit -> Z/Generator_Nom^quantity_Nom\n");
	cryptic_check_good_rc(cryptic_clsig_run_zkpk_schnorr_round1(param2, shn1));
	cryptic_check_good_rc(cryptic_clsig_run_zkpk_schnorr_round1(param2, shn3));

	printf("----> \tProof range interactive first round: same quantity (same random)\n");
	//same quantity, same random
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(random_m,cryptic_zkpk_schnorr_get_i_random(shn1,3)));
	cryptic_check_good_rc(cryptic_proofrange_qrg_round1(pr1,
			CRYPTIC_PROOF_RANGE_LT,
			m,
			b,
			random_m,
			cryptic_clsig_get_lg_sec_param(param2),
			cryptic_clsig_get_lg_zk_sec_param(param2),
			256));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(random_m,cryptic_zkpk_schnorr_get_i_random(shn3,3)));
	cryptic_check_good_rc(cryptic_proofrange_qrg_round1(pr3,
			CRYPTIC_PROOF_RANGE_LT,
			m,
			b,
			random_m,
			cryptic_clsig_get_lg_sec_param(param2),
			cryptic_clsig_get_lg_zk_sec_param(param2),
			256));

	printf("----> \tCompute hash\n");
	hash1 = cryptic_hash_for_ni_proofs_new(256);
	goto_cleanup_if_fail_with_warning(hash1 != NULL);
	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_proof(hash1,shn1,P));
	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_proofrange_prover(hash1,pr1));
	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_compute_hash(hash1));





	printf("\n\n5. *** VERIFIER (for interactive proof)\n");

	printf("----> \tProof initialization\n");
	shn2 = cryptic_zkpk_schnorr_new(tab_gene, 6, cryptic_clsig_get_modulus(param3));
	goto_cleanup_if_fail_with_warning(shn2 != NULL);
	shn4 = cryptic_zkpk_schnorr_new(tab_gene, 6, cryptic_clsig_get_modulus(param3));
	goto_cleanup_if_fail_with_warning(shn4 != NULL);
	pr2 = cryptic_proofrange_qrg_new(cryptic_clsig_get_Z(param3),cryptic_clsig_get_S(param3),cryptic_clsig_get_modulus(param3));
	goto_cleanup_if_fail_with_warning(pr2 != NULL);
	pr4 = cryptic_proofrange_qrg_new(cryptic_clsig_get_Z(param3),cryptic_clsig_get_S(param3),cryptic_clsig_get_modulus(param3));
	goto_cleanup_if_fail_with_warning(pr4 != NULL);

	printf("----> \tCompute Challenge\n");
	cryptic_check_good_rc(cryptic_find_random(challenge,cryptic_clsig_get_lg_quantities(param3)));





	printf("\n\n6. *** PROVER\n");

	printf("----> \tProof interactive second round\n");
	cryptic_check_good_rc(cryptic_zkpk_schnorr_round2_without_order(shn1,cryptic_hash_for_ni_proofs_get_hash(hash1),tab_qty));
	cryptic_check_good_rc(cryptic_zkpk_schnorr_round2_without_order(shn3,challenge,tab_qty));

	printf("----> \tProof range interactive second round\n");
	cryptic_check_good_rc(cryptic_proofrange_qrg_round2(pr1,cryptic_hash_for_ni_proofs_get_hash(hash1)));
	cryptic_check_good_rc(cryptic_proofrange_qrg_round2(pr3,challenge));





	printf("\n\n7. *** VERIFIER\n");

	printf("----> \tVerify non interactive proof\n");
	tmp = cryptic_zkpk_schnorr_get_responses(shn1);
	goto_cleanup_if_fail_with_warning(tmp != NULL);
	/* tester avec s = r + cx pour les réponses et inverser la rep en DL dans le verify*/
	z = 1;
	for(i=2;i<6;i++){
		goto_cleanup_if_fail_with_warning(tmp[i] != NULL);
		if(BN_num_bits(tmp[i]) >= (2*cryptic_clsig_get_lg_quantities(param3) + cryptic_clsig_get_lg_zk_sec_param(param3) + 1)) z = 0;
	}
	if(BN_num_bits(tmp[0]) >= (cryptic_clsig_get_interval_exponent(param3) + cryptic_clsig_get_lg_zk_sec_param(param3) + cryptic_clsig_get_lg_quantities(param3) + 1)) z = 0;
	j = 0;
	if(!z){
		printf("\t****** PROOF REJECTED: Size of responses for proof 1 too large!\n");
	}else{
		s1 = cryptic_zkpk_schnorr_get_i_response(shn1,3);
		goto_cleanup_if_fail_with_warning(s1 != NULL);
		s2 = cryptic_proofrange_qrg_get_responses(pr1);
		goto_cleanup_if_fail_with_warning(s2 != NULL);
		if(!BN_ucmp(s1,s2[8])){
			cryptic_check_good_rc(cryptic_zkpk_schnorr_verify_noninteractive_proof(shn2,P, cryptic_hash_for_ni_proofs_get_hash(hash1),cryptic_zkpk_schnorr_get_responses(shn1)));

			cryptic_check_good_rc(cryptic_proofrange_qrg_verify_noninteractive_proof(
				pr2,
				CRYPTIC_PROOF_RANGE_LT,
				b,
				cryptic_proofrange_qrg_get_dlreps(pr1),
				cryptic_hash_for_ni_proofs_get_hash(hash1),
				cryptic_proofrange_qrg_get_responses(pr1)));

			hash2 = cryptic_hash_for_ni_proofs_new(256);
			goto_cleanup_if_fail_with_warning(hash2 != NULL);

			cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_proof(hash2,shn2,P));
			cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_proofrange_verifier(hash2,pr2,cryptic_proofrange_qrg_get_dlreps(pr1)));
			cryptic_check_good_rc(cryptic_hash_for_ni_proofs_compute_hash(hash2));

			/* Verify the hash computed is the same as the one received */
			if(!BN_ucmp(cryptic_hash_for_ni_proofs_get_hash(hash1),cryptic_hash_for_ni_proofs_get_hash(hash2))){
				printf("\t****** PROOF ACCEPTED\n");
			}else{
				printf("\t****** PROOF REJECTED\n");
			}
			cryptic_check_good_rc(cryptic_print_bn("hash1: ",cryptic_hash_for_ni_proofs_get_hash(hash1)));
			cryptic_check_good_rc(cryptic_print_bn("hash2: ",cryptic_hash_for_ni_proofs_get_hash(hash2)));


		}else{
			printf("\t****** PROOF 2 REJECTED: Not the same quantity\n");
		}

	}

	printf("----> \tVerify interactive proof\n");
	tmp = cryptic_zkpk_schnorr_get_responses(shn3);
	goto_cleanup_if_fail_with_warning(tmp != NULL);
	z = 1;
	for(i=2;i<6;i++){
		goto_cleanup_if_fail_with_warning(tmp[i] != NULL);
		if(BN_num_bits(tmp[i]) >= (2*cryptic_clsig_get_lg_quantities(param3) + cryptic_clsig_get_lg_zk_sec_param(param3) + 1)) z = 0;
	}
	if(BN_num_bits(tmp[0]) >= (cryptic_clsig_get_interval_exponent(param3) + cryptic_clsig_get_lg_zk_sec_param(param3) + cryptic_clsig_get_lg_quantities(param3) + 1)) z = 0;
	j = 1;
	if(!z){
		printf("\t****** PROOF 1 REJECTED: Size of responses too large!\n"); j = 0;
	}else{
		if(cryptic_zkpk_schnorr_verify_interactive_proof(shn4,P, shn3->commitment, challenge,cryptic_zkpk_schnorr_get_responses(shn3)) == 1){
			printf("\t****** PROOF 1 ACCEPTED\n");
		}else{
			printf("\t****** PROOF 1 REJECTED\n");
			j = 0;
		}
	}
	s1 = cryptic_zkpk_schnorr_get_i_response(shn3,3);
	goto_cleanup_if_fail_with_warning(s1 != NULL);
	s2 = cryptic_proofrange_qrg_get_responses(pr3);
	goto_cleanup_if_fail_with_warning(s2 != NULL);
	if(!BN_ucmp(s1,s2[8])){
		if(cryptic_proofrange_qrg_verify_interactive_proof(
			pr4,
			CRYPTIC_PROOF_RANGE_LT,
			b,
			cryptic_proofrange_qrg_get_dlreps(pr3),
			cryptic_proofrange_qrg_get_commitments(pr3),
			challenge,
			cryptic_proofrange_qrg_get_responses(pr3)) == 1){
			printf("\t****** PROOF 2 ACCEPTED\n");
		}else{
			printf("\t****** PROOF 2 REJECTED\n");
			j = 0;
		}
	}else{
		printf("\t****** PROOF 2 REJECTED\n");
		j = 0;
	}
	if(j){
		printf("\t****** PROOF ACCEPTED\n\n");
	}else{
		printf("\t****** PROOF REJECTED\n\n");
	}

	rc = CRYPTIC_NO_ERROR;
cleanup:
	cryptic_release_ctx(ctx);
	g_free(i_q);
	g_free(i_b);
	cryptic_release_gobject(param1);
	cryptic_release_gobject(param2);
	cryptic_release_gobject(param3);
	cryptic_release_gobject(shn1);
	cryptic_release_gobject(shn2);
	cryptic_release_gobject(shn3);
	cryptic_release_gobject(shn4);
	cryptic_release_gobject(pr1);
	cryptic_release_gobject(pr2);
	cryptic_release_gobject(pr3);
	cryptic_release_gobject(pr4);
	cryptic_release_gobject(hash1);
	cryptic_release_gobject(hash2);
	for(i=0;i<nb_messages+2;i++){
		cryptic_release_bn(tab_gene[i]);
		cryptic_release_bn(tab_qty[i]);
	}
	cryptic_release_bn(tmp1);
	cryptic_release_bn(P);
	cryptic_release_bn(m);
	cryptic_release_bn(b);
	cryptic_release_bn(challenge);
	cryptic_release_bn(random_m);
	return rc;
}

int test_3(){
	int rc = CRYPTIC_ERROR_UNDEFINED;

	printf("\n");
	printf("------******------------------------------------------------------------------******------\n");
	printf("------******------------------------------- NI ZKPK --------------------------******------\n");
	printf("------******------------------------------------------------------------------******------\n\n");

	int i;
	CrypticPrimeOrderGroup *g = NULL;
	CrypticZkpkSchnorr *shn1 = NULL, *shn2 = NULL;
	CrypticHashForNiProofs *hash1 = NULL, *hash2 = NULL;
	BIGNUM* tmp = NULL, *m = NULL, *dlrep = NULL;
	BIGNUM *tab_qty[3];
	BN_CTX *ctx = NULL;
	BN_MONT_CTX *mont = NULL;
	char* str[3];
	for(i=0; i<3; i++){
		tab_qty[i] = NULL;
		str[i] = NULL;
	}

	goto_cleanup_if_fail_with_rc_with_warning_openssl(tmp = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(m = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(dlrep = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(dlrep,1));
	goto_cleanup_if_fail_with_rc_with_warning_openssl(tab_qty[0] = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(tab_qty[1] = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(tab_qty[2] = BN_new());

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ctx = BN_CTX_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(mont=BN_MONT_CTX_new());

	str[0]="Gui";
	printf("PRENOM:\t\t\t%s\n",str[0]);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_bin2bn((const unsigned char *) str[0],strlen(str[0]),tab_qty[0]));
	cryptic_check_good_rc(cryptic_print_bn("\t\t\t",tab_qty[0]));

	str[1]="Montag";
	printf("NOM:\t\t\t%s\n",str[1]);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_bin2bn((const unsigned char *) str[1],strlen(str[1]),tab_qty[1]));
	cryptic_check_good_rc(cryptic_print_bn("\t\t\t",tab_qty[1]));

	str[2]="1950010100";
	printf("DATE DE NAISSANCE:\t%s\n",str[2]);
	//Put an int not a string!
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(m,1982012803) == 1);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_copy(tab_qty[2],m));
	cryptic_check_good_rc(cryptic_print_bn("\t\t\t",tab_qty[2]));

	g = cryptic_prime_order_group_new(512); //TODO: Why 345 sometimes fails?
	goto_cleanup_if_fail_with_warning(g != NULL);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_MONT_CTX_set(mont,g->p,ctx));

	cryptic_check_good_rc(cryptic_prime_order_group_more_bases(g,3));

	for(i=0;i<3;i++){
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_mod_exp(tmp,g->bases[i],tab_qty[i],g->p,ctx) == 1);
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_mod_mul(dlrep,dlrep,tmp,g->p,ctx) == 1);
	}



	printf("\n\n1. *** PROVER\n");
	/* Proof for prover */
	shn1 = cryptic_zkpk_schnorr_new(g->bases, 3, g->p);
	goto_cleanup_if_fail_with_warning(shn1 != NULL);

	/* Compute commitment */
	cryptic_check_good_rc(cryptic_zkpk_schnorr_round1(shn1));

	/* Compute hash for all proofs */
	hash1 = cryptic_hash_for_ni_proofs_new(256);
	goto_cleanup_if_fail_with_warning(hash1 != NULL);

	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_modulus(hash1,g->p));
	for(i=0;i<3;i++){
		cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_base(hash1,g->bases[i]));
	}
	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_dlrep(hash1,dlrep));
	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_commitment(hash1,shn1->commitment));
	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_compute_hash(hash1));

	/* Compute responses with the hash*/
	cryptic_check_good_rc(cryptic_zkpk_schnorr_round2_without_order(shn1,cryptic_hash_for_ni_proofs_get_hash(hash1),tab_qty));




	printf("\n\n2. *** VERIFIER\n");
	/* Proof for verifier */
	shn2 = cryptic_zkpk_schnorr_new(g->bases, 3, g->p);
	goto_cleanup_if_fail_with_warning(shn2 != NULL);

	/* Compute commitment with the hash and responses received*/
	cryptic_check_good_rc(cryptic_zkpk_schnorr_verify_noninteractive_proof(shn2, dlrep, cryptic_hash_for_ni_proofs_get_hash(hash1), shn1->responses));

	/* Compute hash for all proofs */
	hash2 = cryptic_hash_for_ni_proofs_new(256);
	goto_cleanup_if_fail_with_warning(hash2 != NULL);

	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_modulus(hash2,g->p));
	for(i=0;i<3;i++){
		cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_base(hash2,g->bases[i]));
	}	
	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_dlrep(hash2,dlrep));
	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_add_commitment(hash2,shn2->commitment));
	cryptic_check_good_rc(cryptic_hash_for_ni_proofs_compute_hash(hash2));

	/* Verify the hash computed is the same as the one received */
	cryptic_check_good_rc(cryptic_print_bn("hash1: ",cryptic_hash_for_ni_proofs_get_hash(hash1)));
	cryptic_check_good_rc(cryptic_print_bn("hash2: ",cryptic_hash_for_ni_proofs_get_hash(hash2)));




	rc = CRYPTIC_NO_ERROR;
cleanup:
	cryptic_release_ctx(ctx);
	cryptic_release_mont(mont);
	for(i=0;i<3;i++){
		cryptic_release_bn(tab_qty[i]);
	}
	cryptic_release_bn(m);
	cryptic_release_bn(tmp);
	cryptic_release_bn(dlrep);
	cryptic_release_gobject(g);
	cryptic_release_gobject(hash1);
	cryptic_release_gobject(hash2);
	cryptic_release_gobject(shn1);
	cryptic_release_gobject(shn2);
	return rc;
}

int main(int argc, char **argv) {

	printf("\n\n\n");
	printf("----------------------------------------***********---------------------------------------\n");
	printf("------******------------------------------------------------------------------******------\n");
	printf("------****---------------------------- Cryptic Tests ---------------------------****------\n");
	printf("------****--------------------------------- v0.1 -------------------------------****------\n");
	printf("------******------------------------------------------------------------------******------\n");
	printf("----------------------------------------***********---------------------------------------\n");

	int rc = CRYPTIC_ERROR_UNDEFINED;

	cryptic_init();

	//activ_CB();
	rc = test_1();
	if(rc != 0){
		return(rc);
	}
	rc = test_2();
	if(rc != 0){
		return(rc);
	}
	rc = test_3();
	return(rc);
}

static int MS_CALLBACK qrn_cb(int p, int n, BN_GENCB *arg){
	char c='*';
	static int ok=0,num=0;

	if (p == 0) { c='.'; num++; };
	if (p == 1) c='+';
	if (p == 2) { c='*'; ok++; }
	if (p == 3) c='\n';
	BIO_write(arg->arg,&c,1);
	(void)BIO_flush(arg->arg);

	if (!ok && (p == 0) && (num > 1))
		{
		BIO_printf((BIO *)arg,"Error in CB tests.\n");
		return(0);
		}
	return(1);
}

