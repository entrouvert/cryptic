/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2011 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/* $ javac -g Myclass.java
   $ LD_LIBRARY_PATH=.libs/ gij -classpath cryptic.jar:. Myclass*/

import com.entrouvert.cryptic.*;

class Myclass{
    public static void main(String[] arg){
	test_1();
    }

    private static void test_1(){
	System.out.println("\n\t-------------- JAVA BINDING TEST --------------\n");

	int rc;

	System.out.println("\nServer: clsig parameters generation...");
	Clsig c = new Clsig(1024,256,600,0,0,0,10);
	rc = c.generateParameters();
	System.out.println("\trc: " + rc);
	System.out.println("Server: clsig parameters generated.");

	System.out.println("\nProver: clsig parameters init on the prover side.");
	Clsig c2;
	c2 = Clsig.newLoadPublicParameters(c.getz(),c.gets(),10,c.getbases(),c.getlgQuantities(),c.getlgExponent(),c.getmodulus(),c.getlgSecParam(),c.getlgZkSecParam(),c.getlgClsigSecParam());

	System.out.println("Prover: build a commitment to include in the certificate.");
	String qa = CrypticJNI.char_to_bn("Kirk");
	String qb = CrypticJNI.char_to_bn("Captain");
	String[] tab_q = {qa, qb};
	System.out.println("Prover: Attributes encoding...");
	System.out.println("\tKirk - " + tab_q[0]);
	System.out.println("\tCaptain - " + tab_q[1]);

	System.out.println("Prover: Init commitment store with bases: 1, 5");
	CommitDataStore pdc = new CommitDataStore();
	String[] tab_b = {(String)c2.getbases()[1],(String)c2.getbases()[5]};
	System.out.println("Prover: Compute commitment");
	rc = c2.computeCommittedValue(pdc,tab_b,tab_q,2);
	System.out.println("\trc: " + rc);

	System.out.println("\nServer: Attributes in certificate.");
	String q1 = CrypticJNI.char_to_bn("Mik");
	String q2 = CrypticJNI.char_to_bn("Ates");
	String q3 = CrypticJNI.char_to_bn("Clermont-Fd");
	String q4 = CrypticJNI.int_to_bn(1982280100);
	String[] tab_q2 = {q1, q2, q3, q4};
	System.out.println("\tMik - " + tab_q2[0]);
	System.out.println("\tAtes - " + tab_q2[1]);
	System.out.println("\tClermont-Fd - " + tab_q2[2]);
	System.out.println("\t1982280100 - " + tab_q2[3]);

	int[] index = {0,2,3,4};
	System.out.println("Server: compute dlrep");
	rc = c.computeDlrepByIndex(tab_q2, index, 4);
	System.out.println("\trc: " + rc);

	System.out.println("Server: sign with the commitment");
	rc = c.signWithCommittedValue(pdc.getdlrep());
	System.out.println("\trc: " + rc);
	System.out.println("Server: verify signature");
	rc = c.verifySignatureNotRandomized();
	System.out.println("Server: signature verification: " + rc);
	System.out.println("\tSignature: " + c.geta());
	System.out.println("\tExponent: " + c.gete());
	System.out.println("\tBlind factor: " + c.getv());

	System.out.println("\nProver: load certificate");
	rc = c2.loadCertificateWithIndexWithCommittedValue(c.geta(),c.gete(),c.getv(),c.getquantities(),4,tab_q,2,pdc.getdlrep(),pdc.getvprime(),index);
	System.out.println("Prover: rc " + rc);
	System.out.println("Prover: randomize signature");
	rc = c2.randomizeSignature();
	System.out.println("\trc: " + rc);
	System.out.println("Prover: verify randomized signature");
	rc = c2.verifySignatureRandomized();
	System.out.println("Prover: signature verification: " + rc);
	System.out.println("Prover: get DLREP to prove");
	System.out.println("Prover: the name will be revealed");
	int[] i1 = {1};
	int[] i2 = {2};
	String dlrep = c2.buildDlrepBeforeProving(i1, i2, 1);

	System.out.println("Prover: compute randoms");
	int fixed_add_size = c2.getlgZkSecParam() + c2.getlgQuantities();
	String r0 = CrypticJNI.ret_random(c2.getintervalExponent() + fixed_add_size);
	String r1 = CrypticJNI.ret_random(c2.getlgBlind() + fixed_add_size);
	String r2 = CrypticJNI.ret_random(c2.getlgQuantities() + fixed_add_size);
	String r3 = CrypticJNI.ret_random(c2.getlgQuantities() + fixed_add_size);
	String r4 = CrypticJNI.ret_random(c2.getlgQuantities() + fixed_add_size);
	String r5 = CrypticJNI.ret_random(c2.getlgQuantities() + fixed_add_size);
	String r6 = CrypticJNI.ret_random(c2.getlgQuantities() + fixed_add_size);

	System.out.println("Prover: build proof");
	String[] tab_b2 = {c2.getaRand(),c2.gets(),c2.getbases()[0],c2.getbases()[3],c2.getbases()[4],c2.getbases()[1],c2.getbases()[5]};
	ZkpkSchnorr s = new ZkpkSchnorr(tab_b2,7,c2.getmodulus());
	String[] tab_r = {r0,r1,r2,r3,r4,r5,r6};
	System.out.println("Prover: proof round 1");
	rc = s.round1RandomsChosen(tab_r);
	System.out.println("\trc: " + rc);

	System.out.println("Prover: build proof range");
	ProofrangeQrg pr = new ProofrangeQrg(c2.gets(),c2.getz(),c2.getmodulus());
	System.out.println("Prover: proof range round 1");
	String b = CrypticJNI.int_to_bn(1992040400);
	rc = pr.round1(CrypticJNI.CRYPTIC_PROOF_RANGE_LT_get(),q4,b,s.getrandoms()[4],c2.getlgSecParam(),c2.getlgZkSecParam(),256);
	System.out.println("\trc: " + rc);

	System.out.println("Prover: build hash for non interactive proof");
	HashForNiProofs h = new HashForNiProofs(256);
	rc = h.addProof(s,dlrep);
	System.out.println("\trc: " + rc);
	rc = h.addProofrangeProver(pr);
	System.out.println("\trc: " + rc);
	rc = h.computeHash();
	System.out.println("\trc: " + rc);

	String[] tab_q3 = {c2.geteCorrected(),c2.getvRand(),q1,q3,q4,qa,qb};
	System.out.println("Prover: proof round 2");
	rc = s.round2WithoutOrder(h.gethValue(),tab_q3);
	System.out.println("\trc: " + rc);
	System.out.println("Prover: proof range round 2");
	rc = pr.round2(h.gethValue());
	System.out.println("\trc: " + rc);

	System.out.println("\nVerifier: clsig parameters init on the verifier side.");
	Clsig c3;
	c3 = Clsig.newLoadPublicParameters(c.getz(),c.gets(),10,c.getbases(),c.getlgQuantities(),c.getlgExponent(),c.getmodulus(),c.getlgSecParam(),c.getlgZkSecParam(),c.getlgClsigSecParam());
	String[] tab_b3 = {c2.getaRand(),c3.gets(),c3.getbases()[0],c3.getbases()[3],c3.getbases()[4],c2.getbases()[1],c2.getbases()[5]};

	System.out.println("Verifier: build proof");
	ZkpkSchnorr s2 = new ZkpkSchnorr(tab_b3,7,c3.getmodulus());
	System.out.println("Verifier: verify proof");
	rc = s2.verifyNoninteractiveProof(dlrep,h.gethValue(),s.getresponses());
	System.out.println("\trc: " + rc);

	System.out.println("Verifier: build proof range");
	ProofrangeQrg pr2 = new ProofrangeQrg(c3.gets(),c3.getz(),c3.getmodulus());
	System.out.println("Verifier: verify proof range");
	rc = pr2.verifyNoninteractiveProof(CrypticJNI.CRYPTIC_PROOF_RANGE_LT_get(),b,pr.getdlreps(),h.gethValue(),pr.getresponses());
	System.out.println("\trc: " + rc);

	System.out.println("Verifier: add proof and proof range to the hash for non interactive proof");
	HashForNiProofs h2 = new HashForNiProofs(256);
	rc = h2.addProof(s2,dlrep);
	System.out.println("\trc: " + rc);
	rc = h2.addProofrangeVerifier(pr2,pr.getdlreps());
	System.out.println("\trc: " + rc);
	rc = h2.computeHash();
	System.out.println("\trc: " + rc);

	rc = CrypticJNI.cmp_bn(h.gethValue(),h2.gethValue());
	System.out.println("Verifier: Proof verification: " + rc);
   }
}

