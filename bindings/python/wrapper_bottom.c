PyMODINIT_FUNC
init_cryptic(void)
{
	PyObject *m, *d;

	if (PyType_Ready(&PyGObjectPtrType) < 0)
		return;

	m = Py_InitModule3("_cryptic", cryptic_methods, "_cryptic wrapper module");
        d = PyModule_GetDict(m);
        register_constants(d);

	cryptic_wrapper_key = g_quark_from_static_string("PyLasso::wrapper");

	Py_INCREF(&PyGObjectPtrType);
	PyModule_AddObject(m, "PyGObjectPtr", (PyObject *)&PyGObjectPtrType);
}

