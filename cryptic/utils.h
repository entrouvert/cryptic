/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_UTILS_H
#define CRYPTIC_UTILS_H

#include <glib.h>
#include <openssl/bn.h>

#define cryptic_critical(message, args...) \
	g_log("cryptic", G_LOG_LEVEL_CRITICAL, message, ## args)

#define cryptic_warning(message, args...) \
	g_log("cryptic", G_LOG_LEVEL_WARNING, message, ## args)

#define cryptic_message(message, args...) \
	g_log("cryptic", G_LOG_LEVEL_MESSAGE, message, ## args)

/**
 * cryptic_ref:
 * @object: an object whose reference count must be incremented.
 *
 * Increment the reference count of an object, do not emit warning if it is NULL.
 *
 * Return value: the @object.
 */
#define cryptic_ref(object) ((object) != NULL ? (g_object_ref(object), object) : NULL)

/**
 * cryptic_unref:
 * @object: an object whose reference count must be decremented.
 * 
 * Decrement the reference count of an object, do not emit warnings if it is NULL.
 *
 * Return value: the @object.
 */
#define cryptic_unref(object) ((object) != NULL ? (g_object_unref(object), object) : NULL)

/* Freeing */

/*
 * cryptic_release_xxx are macros which ensure you do not get 'double free' errors, they first check
 * that the variable is not NULL before calling the deallocation function, and after deallocation
 * they reset the variable to NULL, preventing 'double free'.
 */
#define cryptic_release(dest) \
	{ \
		if (dest) { \
			g_free(dest); dest = NULL; \
		} \
	}

#define cryptic_release_bn(bn) \
	{ \
		BIGNUM **__tmp = &(bn); \
		if (*__tmp) { \
			BN_clear_free(*__tmp); \
			*__tmp = NULL; \
		} \
	}

//BN_CTX_end(ctx) produce a seg fault;
#define cryptic_release_ctx(ctx) \
	{ \
		if (ctx) { \
			BN_CTX_free(ctx); \
		} \
	}

#define cryptic_release_mont(mont) \
	{ \
		if (mont != NULL) { \
			BN_MONT_CTX_free(mont); \
		} \
	}

#define cryptic_release_full(dest, free_function) \
	{ \
		if (dest) { \
			free_function(dest); dest = NULL; \
		} \
	}

#define cryptic_release_full2(dest, free_function, type) \
	{ \
		cryptic_check_type_equality(dest, type); \
		if (dest) { \
			free_function(dest); dest = NULL; \
		} \
	}

#define cryptic_release_gobject(dest) \
	{ \
		if (G_IS_OBJECT(dest) || dest == NULL) { \
			cryptic_release_full(dest, g_object_unref); \
		} else { \
			g_critical("Trying to unref a non GObject pointer file=%s:%u pointerbybname=%s pointer=%p", __FILE__, __LINE__, #dest, dest); \
		} \
	}

#define cryptic_release_string(dest) \
	cryptic_release_full(dest, g_free)

#define cryptic_release_list(dest) \
	cryptic_release_full2(dest, g_list_free, GList*)

#define cryptic_release_list_of_full(dest, free_function) \
	{ \
		GList **__tmp = &(dest); \
		if (*__tmp) { \
			g_list_foreach(*__tmp, (GFunc)free_function, NULL); \
			cryptic_release_list(*__tmp); \
		} \
	}

#define cryptic_release_list_of_strings(dest) \
	cryptic_release_list_of_full(dest, g_free)

#define cryptic_release_list_of_gobjects(dest) \
	cryptic_release_list_of_full(dest, g_object_unref)

#define cryptic_release_ghashtable(dest) \
	cryptic_release_full(dest, g_hash_table_destroy)

#define cryptic_release_gstring(dest, b) \
	{ \
		GString **__tmp = &(dest); \
		if (*__tmp) {\
			g_string_free(*__tmp, (b)); \
			*__tmp = NULL; \
		} \
	}

/* Assignment and list appending */
/*
 * cryptic_assign_xxx macros ensure that you dot leak previous value of assigned things, they use
 * cryptic_release_xxx macros to deallocate, they also ensure proper reference counting on passed by
 * references values and proper copying on passed by value values.
 */
#define cryptic_assign_string(dest,src) \
	{ \
		char *__tmp = g_strdup(src);\
		cryptic_release_string(dest); \
		dest = __tmp; \
	}

#define cryptic_assign_new_string(dest,src) \
	{ \
		char *__tmp = src; \
		if (dest != __tmp) \
			cryptic_release_string(dest); \
		dest = __tmp; \
	}

#define cryptic_assign_gobject(dest,src) \
	{ \
		GObject *__tmp = G_OBJECT(src); \
		if (__tmp) \
			g_object_ref(__tmp); \
		cryptic_release_gobject(dest); \
		dest = (void*)(__tmp); \
	}

#define cryptic_assign_new_gobject(dest,src) \
	{ \
		GObject *__tmp = G_OBJECT(src); \
		if (dest != (void*)__tmp) \
			cryptic_release_gobject(dest); \
		dest = (void*)(__tmp); \
	}

#define cryptic_assign_list(dest, src) \
	{ \
		GList **__tmp = &(dest); \
		if (*__tmp) \
			g_list_free(*__tmp); \
		*__tmp = g_list_copy((src)); \
	}

#define cryptic_assign_new_list_of_gobjects(dest, src) \
	{ \
		GList *__tmp = (src); \
		cryptic_release_list_of_gobjects(dest); \
		dest = (GList*)__tmp; \
	}

#define cryptic_assign_new_list_of_strings(dest, src) \
	{ \
		GList *__tmp = (src); \
		cryptic_release_list_of_strings(dest); \
		dest = (GList*)__tmp; \
	}

#define cryptic_assign_list_of_gobjects(dest, src) \
	{ \
		GList *__tmp = (src); \
		cryptic_release_list_of_gobjects(dest); \
		dest = g_list_copy(__tmp); \
		for (;__tmp != NULL; __tmp = g_list_next(__tmp)) { \
			if (G_IS_OBJECT(__tmp->data)) { \
				g_object_ref(__tmp->data); \
			} \
		} \
	}

#define cryptic_assign_list_of_strings(dest, src) \
	{ \
		GList *__tmp = src; \
		GList *__iter_dest; \
		cryptic_release_list_of_strings(dest); \
		dest = g_list_copy(__tmp); \
		for (__iter_dest = dest ; __iter_dest != NULL ; __iter_dest = g_list_next(__iter_dest)) { \
			__iter_dest->data = g_strdup(__iter_dest->data); \
		} \
	}

/* List appending */

/* cryptic_list_add_xxx macros, simplify code around list manipulation (g_list_append needs to be
 * used like this 'l = g_list_appen(l, value)' ) and ensure proper reference count or copying of
 * values.
 */
#define cryptic_list_add(dest, src) \
	{ \
		cryptic_check_type_equality((src), void*); \
		dest = g_list_append(dest, (src)); \
	}

#define cryptic_list_add_non_null(dest, src) \
	{ \
		void *__tmp_non_null_src = (src); \
		if (__tmp_non_null_src != NULL) { \
			dest = g_list_append(dest, __tmp_non_null_src); \
		} else { \
			g_critical("Adding a NULL value to a non-NULL content list: dest=%s src=%s", #dest, #src); \
		} \
	}

#define cryptic_list_add_string(dest, src) \
	{ \
		cryptic_list_add_non_null(dest, g_strdup(src));\
	}

#define cryptic_list_add_new_string(dest, src) \
	{ \
		gchar *__tmp = src; \
		cryptic_list_add_non_null(dest, __tmp); \
	}

#define cryptic_list_add_gobject(dest, src) \
	{ \
		void *__tmp_src = (src); \
		if (G_IS_OBJECT(__tmp_src)) { \
			dest = g_list_append(dest, g_object_ref(__tmp_src)); \
		} else { \
			g_critical("Trying to add to a GList* a non GObject pointer dest=%s src=%s", #dest, #src); \
		} \
	}

#define cryptic_list_add_new_gobject(dest, src) \
	{ \
		void *__tmp_src = (src); \
		if (G_IS_OBJECT(__tmp_src)) { \
			dest = g_list_append(dest, __tmp_src); \
		} else { \
			g_critical("Trying to add to a GList* a non GObject pointer dest=%s src=%s", #dest, #src); \
		} \
	}

#define cryptic_list_add_gstrv(dest, src) \
	{ \
		GList **__tmp_dest = &(dest); \
		const char **__iter = (const char**)(src); \
		while (__iter && *__iter) { \
			cryptic_list_add_string(*__tmp_dest, *__iter); \
		} \
	}

/* List element removal */
#define cryptic_list_remove_gobject(list, gobject) \
	do { void *__tmp = gobject; GList **__tmp_list = &(list); \
		*__tmp_list = g_list_remove(*__tmp_list, __tmp); \
		cryptic_unref(__tmp); } while(0)

/* Pointer ownership transfer */

/* cryptic_transfer_xxx macros are like cryptic_assign_xxx but they do not increment reference count or
 * copy the source value, instead they steal the value (and set the source to NULL, preventing stale
 * references).
 */
#define cryptic_transfer_full(dest, src, kind) \
	{\
		cryptic_release_##kind((dest)); \
		cryptic_check_type_equality(dest, src); \
		(dest) = (void*)(src); \
		(src) = NULL; \
	}

#define cryptic_transfer_xpath_object(dest, src) \
	cryptic_transfer_full(dest, src, xpath_object)

#define cryptic_transfer_string(dest, src) \
	cryptic_transfer_full(dest, src, string)

#define cryptic_transfer_gobject(dest, src) \
	cryptic_transfer_full(dest, src, gobject)

/* Node extraction */
#define cryptic_extract_node_or_fail(to, from, kind, error) \
	{\
		void *__tmp = (from); \
		if (CRYPTIC_IS_##kind(__tmp)) { \
			to = CRYPTIC_##kind(__tmp); \
		} else { \
			rc = error; \
			goto cleanup; \
		}\
	}

/* Bad param handling */
#define cryptic_return_val_if_invalid_param(kind, name, val) \
	g_return_val_if_fail(CRYPTIC_IS_##kind(name), val)

#define cryptic_bad_param(kind, name) \
	cryptic_return_val_if_invalid_param(kind, name, \
		CRYPTIC_PARAM_ERROR_BAD_TYPE_OR_NULL_OBJ);

#define cryptic_null_param(name) \
	g_return_val_if_fail(name != NULL, CRYPTIC_PARAM_ERROR_BAD_TYPE_OR_NULL_OBJ);

inline static gboolean
cryptic_is_empty_string(const char *str) {
	return ((str) == NULL || (str)[0] == '\0');
}

/**
 * cryptic_check_non_empty_string:
 * @str: a char pointer
 *
 * Check that @str is non-NULL and not empty, otherwise jump to cleanup and return
 * CRYPTIC_PARAM_ERROR_BAD_TYPE_OR_NULL_OBJ.
 */
#define cryptic_check_non_empty_string(str) \
	goto_cleanup_if_fail_with_rc(! cryptic_is_empty_string(str), \
			CRYPTIC_PARAM_ERROR_BAD_TYPE_OR_NULL_OBJ);

/*
 * We extensively use goto operator but in a formalized way, i.e. only for error checking code
 * paths.
 *
 * The next macros goto_cleanup_xxxx encapsulate idioms used in cryptic, like checking for a condition
 * or setting the return code which must be called 'rc' and be of an 'int' type.
 */

/*
 * The following macros are made to create some formalism for function's cleanup code.
 *
 * The exit label should be called 'cleanup'. And for functions returning an integer error code, the
 * error code should be named 'rc' and 'return rc;' should be the last statement of the function.
 */

/**
 * goto_cleanup_with_rc:
 * @rc_value: integer return value
 *
 * This macro jump to the 'cleanup' label and set the return value to @rc_value.
 *
 */
#define goto_cleanup_with_rc(rc_value) \
	do {\
		rc = (rc_value); \
		goto cleanup; \
	} while(0);

/**
 * goto_cleanup_if_fail:
 * @condition: a boolean condition
 *
 * Jump to the 'cleanup' label if the @condition is FALSE.
 *
 */
#define goto_cleanup_if_fail(condition) \
	{\
		if (! (condition) ) {\
			goto cleanup; \
		} \
	}

/**
 * goto_cleanup_if_fail_with_rc:
 * @condition: a boolean condition
 * @rc_value: integer return value
 *
 * Jump to the 'cleanup' label if the @condition is FALSE and set the return value to
 * @rc_value.
 *
 */
#define goto_cleanup_if_fail_with_rc(condition, rc_value) \
	{\
		if (! (condition) ) {\
			rc = (rc_value); \
			goto cleanup; \
		} \
	}

/**
 * goto_cleanup_if_fail_with_rc_with_warning:
 * @condition: a boolean condition
 * @rc_value: integer return value
 *
 * Jump to the 'cleanup' label if the @condition is FALSE and set the return value to
 * @rc_value. Also emit a warning, showing the condition and the return value.
 *
 */
#define goto_cleanup_if_fail_with_rc_with_warning(condition, rc_value) \
	{\
		if (! (condition) ) {\
			g_warning("%s failed, returning %s", #condition, #rc_value);\
			rc = (rc_value); \
			goto cleanup; \
		} \
	}

#define goto_cleanup_if_fail_with_warning(condition) \
	{\
		if (! (condition) ) {\
			g_warning("%s failed", #condition);\
			goto cleanup; \
		} \
	}

#define goto_cleanup_if_fail_with_rc_with_warning_openssl(condition) \
	{\
		if (! (condition) ) {\
			g_warning("%s failed, returning CRYPTIC_ERROR_SSL", #condition);\
			rc = (CRYPTIC_ERROR_SSL); \
			goto cleanup; \
		} \
	}

/**
 * check_good_rc:
 * @what: a call to a function returning a cryptic error code
 *
 * Check if return code is 0, if not store it in rc and jump to cleanup label.
 */
#define cryptic_check_good_rc(what) \
	{ \
		int __rc = (what);\
		goto_cleanup_if_fail_with_rc(__rc == 0, __rc); \
	}

#define cryptic_mem_debug(who, what, where) \
	{ \
		if (cryptic_flag_memory_debug) \
		fprintf(stderr, "  freeing %s/%s (at %p)\n", who, what, (void*)where); \
	}

/**
 * cryptic_foreach:
 * @_iter: a #GList variable, which will server to traverse @_list
 * @_list: a #GList value, which we will traverse
 *
 * Traverse a #GList list using 'for' construct. It must be followed by a block or a statement.
 */
#define cryptic_foreach(_iter, _list) \
	for (_iter = (_list); _iter; _iter = g_list_next(_iter))

/**
 * cryptic_foreach_full_begin:
 * @_type: the type of the variable @_data
 * @_data: the name of the variable to define to store data values
 * @_iter: the name of the variable to define to store the iterator
 * @_list: the GList* to iterate
 *
 * Traverse a GList* @_list, using @_iter as iteration variable extract data field to variable
 * @_data of type @_type.
 */
#define cryptic_foreach_full_begin(_type, _data, _iter, _list) \
	{ \
		_type _data = NULL; \
		GList *_iter = NULL; \
		for (_iter = (_list); _iter && ((_data = _iter->data), 1); _iter = g_list_next(_iter)) \
		{

#define cryptic_foreach_full_end() \
				} }

/**
 * cryptic_list_get_first_child:
 * @list:(allowed-none): a #GList node or NULL.
 *
 * Return the first child in a list, or NULL.
 */
#define cryptic_list_get_first_child(list) \
	((list) ? (list)->data : NULL)

/*
 * Simplify simple accessors argument checking.
 *
 */
#define cryptic_return_val_if_fail(assertion, value) \
	if (!(assertion)) return (value);

#define cryptic_return_null_if_fail(assertion) \
	cryptic_return_val_if_fail(assertion, NULL)

#define cryptic_return_if_fail(assertion) \
	if (!(assertion)) return;

#define cryptic_trace(args...) \
	if (cryptic_flag_memory_debug) { \
		fprintf(stderr, ## args); \
	}


#endif /* CRYPTIC_UTILS_H */
