/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_H
#define CRYPTIC_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "protocols/clsig/clsig.h"
#include "protocols/clsig/commit_data_store.h"
#include "protocols/pok_schnorr/commitments_utils.h"
#include "protocols/pok_schnorr/schnorr_zkpk.h"
#include "protocols/pok_schnorr/hash_for_ni_proofs.h"
#include "protocols/proof_range/proof_range_in_qrg.h"
#include "maths/group_prime_order.h"
#include "maths/quadratic_residues_group.h"
#include "maths/decompose_integer.h"
#include "maths/maths_utils.h"
#include "utils/print.h"
#include "errors.h"
#include "utils.h"
#include "export.h"

CRYPTIC_EXPORT int cryptic_init(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CRYPTIC_H */
