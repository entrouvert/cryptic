/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/**************************************************************
* - Functions usually return a negative error code on failure
* and 0 either
* - Verify functions return 1 on success and <= 0 on failure
* - Getters return NULL if no member
***************************************************************/

/** CRYPTIC GENERIC ERROR CODES **/
/**
 * CRYPTIC_NO_ERROR:
 *
 * Undefined error.
 */
#define CRYPTIC_NO_ERROR 0
/**
 * CRYPTIC_ERROR_UNDEFINED:
 *
 * Undefined error.
 */
#define CRYPTIC_ERROR_UNDEFINED -1
/**
 * CRYPTIC_ERROR_UNIMPLEMENTED:
 *
 * Unimplemented part.
 */
#define CRYPTIC_ERROR_UNIMPLEMENTED -2
/**
 * CRYPTIC_ERROR_OUT_OF_MEMORY:
 *
 * Out of memory.
 */
#define CRYPTIC_ERROR_OUT_OF_MEMORY -3
/**
 * CRYPTIC_MEMORY_ALLOCATION_FAILURE:
 *
 * Memory allocation failure.
 */
#define CRYPTIC_MEMORY_ALLOCATION_FAILURE -4
/**
 * CRYPTIC_ERROR_CAST_FAILED:
 *
 * Expected GObject class was not found, cast failed
 */
#define CRYPTIC_ERROR_CAST_FAILED -5
/**
 * CRYPTIC_ERROR_GET_MEMBER_FAILURE:
 *
 * Member cannot be returned.
 */
#define CRYPTIC_ERROR_GET_MEMBER_FAILURE -6
/**
 * CRYPTIC_ERROR_BAD_SIGNATURE:
 *
 * Member cannot be returned.
 */
#define CRYPTIC_ERROR_BAD_SIGNATURE -7
/**
 * CRYPTIC_ERROR_FUNCTION_PARAMETER_NOT_ALLOCATED:
 *
 * A function parameter is not allocated.
 */
#define CRYPTIC_ERROR_FUNCTION_PARAMETER_NOT_ALLOCATED -8

/** MATHS **/
/**
 * CRYPTIC_MATHS_NUMBER_NOT_PRIME:
 *
 * The number is not prime.
 */
#define CRYPTIC_MATHS_NUMBER_NOT_PRIME -101

/** PRIME ORDER GROUP **/
/**
 * CRYPTIC_MATHS_PRIME_ORDER_GROUP_NB_BASES_EXCEEDED:
 *
 * Number of new bases too large.
 */
#define CRYPTIC_MATHS_PRIME_ORDER_GROUP_NB_BASES_EXCEEDED -111

/** QR GROUP **/
/**
 * CRYPTIC_MATHS_QR_GROUP_MODULUS_SIZE_NOT_EVEN:
 *
 * The modulus size given for the QR group is not even.
 */
#define CRYPTIC_MATHS_QR_GROUP_MODULUS_SIZE_NOT_EVEN -121
/**
 * CRYPTIC_MATHS_QR_GROUP_MODULUS_SIZE_TOO_SMALL:
 *
 * The modulus size is too small.
 */
#define CRYPTIC_MATHS_QR_GROUP_MODULUS_SIZE_TOO_SMALL -122
/**
 * CRYPTIC_MATHS_QR_GROUP_PICKING_BASE_FAILED:
 *
 * Error picking a QRn base.
 */
#define CRYPTIC_MATHS_QR_GROUP_PICKING_BASE_FAILED -123
/**
 * CRYPTIC_MATHS_QR_GROUP_MISSING_ELEMENT:
 *
 * Missing element for minimal loading.
 */
#define CRYPTIC_MATHS_QR_GROUP_MISSING_ELEMENT -124
/**
 * CRYPTIC_MATHS_QR_GROUP_BASE_UNALLOCATED:
 *
 * Bases not allocated.
 */
#define CRYPTIC_MATHS_QR_GROUP_BASE_UNALLOCATED -125
/**
 * CRYPTIC_MATHS_QR_GROUP_NO_QR_TO_VERIFY:
 *
 * No qr to verify.
 */
#define CRYPTIC_MATHS_QR_GROUP_NO_QR_TO_VERIFY -126
/**
 * CRYPTIC_MATHS_QR_GROUP_NOT_A_QR:
 *
 * Not a qr.
 */
#define CRYPTIC_MATHS_QR_GROUP_NOT_A_QR -127
/**
 * CRYPTIC_MATHS_QR_GROUP_MODULUS_MISSING:
 *
 * The modulus is missing.
 */
#define CRYPTIC_MATHS_QR_GROUP_MODULUS_MISSING -128

/**
 * CRYPTIC_MATHS_DECOMPOSITION_4_SQUARES_FAILED:
 *
 * Decomposition integer in four squares failed.
 */
#define CRYPTIC_MATHS_DECOMPOSITION_4_SQUARES_FAILED -131

/** CLSIG **/
/**
 * CRYPTIC_CLSIG_MODULUS_TOO_SMALL:
 *
 * The modulus size is too small.
 */
#define CRYPTIC_CLSIG_MODULUS_TOO_SMALL -301
/**
 * CRYPTIC_CLSIG_MODULUS_MUST_BE_EVEN:
 *
 * The modulus must be even.
 */
#define CRYPTIC_CLSIG_MODULUS_MUST_BE_EVEN -302
/**
 * CRYPTIC_CLSIG_MESSAGE_SIZE_MUST_BE_LESS_THAN_MODULUS:
 *
 * The message size must be less than the modulus.
 */
#define CRYPTIC_CLSIG_MESSAGE_SIZE_MUST_BE_LESS_THAN_MODULUS -303
/**
 * CRYPTIC_CLSIG_MESSAGE_SIZE_MUST_BE_LESS_THAN_MODULUS:
 *
 * CLSIG not correctly initialized.
 */
#define CRYPTIC_CLSIG_NOT_CORRECTLY_INITIALIZED -304
/**
 * CRYPTIC_CLSIG_PARAMETERS_NOT_CORRECTLY_LOADED:
 *
 * CLSIG parameters not correctly loaded.
 */
#define CRYPTIC_CLSIG_PARAMETERS_NOT_CORRECTLY_LOADED -305
/**
 * CRYPTIC_CLSIG_UNABLE_LOAD_PARAMETERS_ELEMENT_MISSING:
 *
 * Unable to load CLSIG public parameters: element missing.
 */
#define CRYPTIC_CLSIG_UNABLE_LOAD_PARAMETERS_ELEMENT_MISSING -306
/**
 * CRYPTIC_CLSIG_BAD_RSA_KEY_PAIR:
 *
 * Bad RSA Key Pair.
 */
#define CRYPTIC_CLSIG_BAD_RSA_KEY_PAIR -307
/**
 * CRYPTIC_CLSIG_EXPONENTIATION_S_NOT_INVERSIBLE:
 *
 * S exponentiation not inversible, S is surely a bad generator.
 */
#define CRYPTIC_CLSIG_EXPONENTIATION_S_NOT_INVERSIBLE -308
/**
 * CRYPTIC_CLSIG_CHALENGE_TOO_SMALL:
 *
 * The challenge size must be larger or equal to the secutiry parameter.
 */
#define CRYPTIC_CLSIG_CHALENGE_TOO_SMALL -309
/**
 * CRYPTIC_CLSIG_EXPONENT_TOO_SMALL:
 *
 * The exponent is too small.
 */
#define CRYPTIC_CLSIG_EXPONENT_TOO_SMALL -309
/**
 * CRYPTIC_CLSIG_TOO_MUCH_QUANTITIES:
 *
 * Too much quantities asked.
 */
#define CRYPTIC_CLSIG_TOO_MUCH_QUANTITIES -310
/**
 * CRYPTIC_CLSIG_MISSING_QUANTITIES:
 *
 * Missing quantities.
 */
#define CRYPTIC_CLSIG_MISSING_QUANTITIES -311
/**
 * CRYPTIC_CLSIG_QUANTITY_TOO_LARGE:
 *
 * Too large quantity.
 */
#define CRYPTIC_CLSIG_QUANTITY_TOO_LARGE -312
/**
 * CRYPTIC_CLSIG_MISSING_BASES:
 *
 * Missing bases.
 */
#define CRYPTIC_CLSIG_MISSING_BASES -313
/**
 * CRYPTIC_CLSIG_NO_VALID_QUANTITIES_NUMBER:
 *
 * The number of quantity is not positive.
 */
#define CRYPTIC_CLSIG_NO_VALID_QUANTITIES_NUMBER -314
/**
 * CRYPTIC_CLSIG_EXPONENT_BAD_SIZE:
 *
 * The exponent is of bad size.
 */
#define CRYPTIC_CLSIG_EXPONENT_BAD_SIZE -315
/**
 * CRYPTIC_CLSIG_MISSING_SIGN_BLIND_RANDOM:
 *
 * Missing blind value for commitment.
 */
#define CRYPTIC_CLSIG_MISSING_SIGN_BLIND_RANDOM -316
/**
 * CRYPTIC_CLSIG_MISSING_SIGN_BLIND_COMMITMENT:
 *
 * Missing commitment of blinded signed quantities.
 */
#define CRYPTIC_CLSIG_MISSING_SIGN_BLIND_COMMITMENT -317
/**
 * CRYPTIC_CLSIG_SIGNATURE_NOT_CORRECTLY_LOADED:
 *
 * CLSIG singature not correctly loaded.
 */
#define CRYPTIC_CLSIG_SIGNATURE_NOT_CORRECTLY_LOADED -318
/**
 * CRYPTIC_CLSIG_LOADING_CERTIFICATE_MISSING_ELEMENT:
 *
 * Missing element for loading certificate.
 */
#define CRYPTIC_CLSIG_LOADING_CERTIFICATE_MISSING_ELEMENT -319
/**
 * CRYPTIC_CLSIG_SIGNATURE_WITH_COMMIT_MISSING_ELEMENT:
 *
 * Missing commitment for signature.
 */
#define CRYPTIC_CLSIG_SIGNATURE_WITH_COMMIT_MISSING_ELEMENT -320
/**
 * CRYPTIC_CLSIG_SIGNATURE_NOT_VALIDATED_TO_RANDOMIZE:
 *
 * No valid signature to randomize.
 */
#define CRYPTIC_CLSIG_SIGNATURE_NOT_VALIDATED_TO_RANDOMIZE -321
/**
 * CRYPTIC_CLSIG_SIGNATURE_NOT_RANDOMIZED:
 *
 * No valid randomized signature to verify.
 */
#define CRYPTIC_CLSIG_SIGNATURE_NOT_RANDOMIZED -322
/**
 * CRYPTIC_CLSIG_UNABLE_TO_CREATE_QRG:
 *
 * Unable to create a quadratic residue group.
 */
#define CRYPTIC_CLSIG_UNABLE_TO_CREATE_QRG -323
/**
 * CRYPTIC_CLSIG_UNABLE_TO_LOAD_QRG:
 *
 * Unable to load a quadratic residue group.
 */
#define CRYPTIC_CLSIG_UNABLE_TO_LOAD_QRG -324
/**
 * CRYPTIC_QRG_UNABLE_TO_LOAD_QRG_MINIMAL:
 *
 * Missing element for minimal loading.
 */
#define CRYPTIC_QRG_UNABLE_TO_LOAD_QRG_MINIMAL -325
/**
 * CRYPTIC_QRG_MODULUS_SIZE_TOO_SMALL:
 *
 * Missing element for minimal loading.
 */
#define CRYPTIC_QRG_MODULUS_SIZE_TOO_SMALL -326
/**
 * CRYPTIC_QRG_MODULUS_SIZE_NOT_EVEN:
 *
 * Modulus size not even.
 */
#define CRYPTIC_QRG_MODULUS_SIZE_NOT_EVEN -327


/** PROOF GENERIC **/
/**
 * CRYPTIC_PROOF_GENERIC_COMMITMENT_MISSING:
 *
 * No valid commitment value provided.
 */
#define CRYPTIC_PROOF_GENERIC_COMMITMENT_MISSING -501
/**
 * CRYPTIC_PROOF_GENERIC_CHALLENGE_MISSING:
 *
 * No valid challenge value provided.
 */
#define CRYPTIC_PROOF_GENERIC_CHALLENGE_MISSING -502
/**
 * CRYPTIC_PROOF_GENERIC_CHALLENGE_SIZE_NOT_VALID:
 *
 * Challenge Size too small.
 */
#define CRYPTIC_PROOF_GENERIC_CHALLENGE_SIZE_NOT_VALID -503
/**
 * CRYPTIC_PROOF_GENERIC_RESPONSES_MISSING:
 *
 * No valid responses value provided.
 */
#define CRYPTIC_PROOF_GENERIC_RESPONSES_MISSING -504
/**
 * CRYPTIC_PROOF_GENERIC_NB_RESPONSES_NOT_VALID:
 *
 * Number of responses not valid.
 */
#define CRYPTIC_PROOF_GENERIC_NB_RESPONSES_NOT_VALID -505
/**
 * CRYPTIC_PROOF_GENERIC_AT_LEAST_ONE_RESPONSE_MISSING:
 *
 * A response value is missing.
 */
#define CRYPTIC_PROOF_GENERIC_AT_LEAST_ONE_RESPONSE_MISSING -506
/**
 * CRYPTIC_PROOF_GENERIC_NB_QUANTITIES_NULL:
 *
 * The number of quantity cannot be null.
 */
#define CRYPTIC_PROOF_GENERIC_NB_QUANTITIES_NULL -507
/**
 * CRYPTIC_PROOF_GENERIC_BASES_MISSING:
 *
 * At least one base is missing.
 */
#define CRYPTIC_PROOF_GENERIC_BASES_MISSING -508
/**
 * CRYPTIC_PROOF_GENERIC_MODULUS_MISSING:
 *
 * Missing modulus.
 */
#define CRYPTIC_PROOF_GENERIC_MODULUS_MISSING -509
/**
 * CRYPTIC_PROOF_RANDOMS_RESPONSES_MISSING:
 *
 * Some randoms value are missing.
 */
#define CRYPTIC_PROOF_GENERIC_RANDOMS_MISSING -510
/**
 * CRYPTIC_PROOF_GENERIC_DLREP_MISSING:
 *
 * Missing DL representation.
 */
#define CRYPTIC_PROOF_GENERIC_DLREP_MISSING -511
/**
 * CRYPTIC_PROOF_GENERIC_ROUND1_NOT_DONE:
 *
 * Round 1 - building commitment - not done.
 */
#define CRYPTIC_PROOF_GENERIC_ROUND1_NOT_DONE -512
/**
 * CRYPTIC_PROOF_GENERIC_HASH_OR_CHALLENGE_MISSING:
 *
 * Hash or challenge missing.
 */
#define CRYPTIC_PROOF_GENERIC_HASH_OR_CHALLENGE_MISSING -513
/**
 * CRYPTIC_PROOF_GENERIC_ORDER_MISSING:
 *
 * Order missing.
 */
#define CRYPTIC_PROOF_GENERIC_ORDER_MISSING -514
/**
 * CRYPTIC_PROOF_GENERIC_STRUCTURE_NOT_INIT:
 *
 * The structure is not well initialized.
 */
#define CRYPTIC_PROOF_GENERIC_STRUCTURE_NOT_INIT -515
/**
 * CRYPTIC_PROOF_GENERIC_QUANTITY_MISSING:
 *
 * Missing quantity.
 */
#define CRYPTIC_PROOF_GENERIC_QUANTITY_MISSING -516

/** ZKPK INTERACTIVE SCHNORR **/
/**
 * CRYPTIC_ZKPK_INTERACTIVE_SCHNORR_STRUCTURE_NOT_INIT:
 *
 * The zkpk interactive schnorr structure is not well initialized.
 */
#define CRYPTIC_ZKPK_INTERACTIVE_SCHNORR_STRUCTURE_NOT_INIT -601
/**
 * CRYPTIC_ZKPK_INTERACTIVE_SCHNORR_RANDOMS_MISSING:
 *
 * Missing randoms to compute commitment.
 */
#define CRYPTIC_ZKPK_INTERACTIVE_SCHNORR_RANDOMS_MISSING -602
/**
 * CRYPTIC_ZKPK_INTERACTIVE_SCHNORR_NUMBER_OF_RANDOMS_MISMATCH:
 *
 * Number of randoms passed is not the sames as the number of bases.
 */
#define CRYPTIC_ZKPK_INTERACTIVE_SCHNORR_NUMBER_OF_RANDOMS_MISMATCH -603
/**
 * CRYPTIC_ZKPK_NONINTERACTIVE_SCHNORR_BAD_HASH_SIZE:
 *
 * Hash size not supported.
 */
#define CRYPTIC_ZKPK_NONINTERACTIVE_SCHNORR_BAD_HASH_SIZE -651

/** PROOF RANGE **/
/**
 * CRYPTIC_PROOF_RANGE_STRUCTURE_NOT_INIT:
 *
 * The proof range structure is not well initialized.
 */
#define CRYPTIC_PROOF_RANGE_STRUCTURE_NOT_INIT -701
/**
 * CRYPTIC_PROOF_RANGE_NO_QUANTITY:
 *
 * No quantity on which a range proof can be led.
 */
#define CRYPTIC_PROOF_RANGE_NO_QUANTITY -702
/**
 * CRYPTIC_PROOF_RANGE_STRUCTURE_NO_BOUND:
 *
 * No bound on which a range proof can be led.
 */
#define CRYPTIC_PROOF_RANGE_NO_BOUND -703
/**
 * CRYPTIC_PROOF_RANGE_STRUCTURE_RELATION_UNKNOWN:
 *
 * Proof range relation unknown.
 */
#define CRYPTIC_PROOF_RANGE_RELATION_UNKNOWN -704
/**
 * CRYPTIC_PROOF_RANGE_DELTA_NEGATIVE:
 *
 * Proof range delta negative.
 */
#define CRYPTIC_PROOF_RANGE_DELTA_NEGATIVE -705
/**
 * CRYPTIC_PROOF_RANGE_DECOMPOSITION_FAILED:
 *
 * Proof range decomposition failed.
 */
#define CRYPTIC_PROOF_RANGE_DECOMPOSITION_FAILED -706
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_TAB_DLREPS_EMPTY:
 *
 * Missing tab of representations.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_TAB_DLREPS_EMPTY -707
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_TAB_COMMITMENTS_EMPTY:
 *
 * Missing tab of commitments.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_TAB_COMMITMENTS_EMPTY -708
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_TAB_RESPONSES_EMPTY:
 *
 * Missing tab of responses.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_TAB_RESPONSES_EMPTY -709
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_DLREP_FIRST_SQRROOT_MISSING:
 *
 * Missing representation of the first square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_DLREP_FIRST_SQRROOT_MISSING -710
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_DLREP_SECOND_SQRROOT_MISSING:
 *
 * Missing representation of the SECOND square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_DLREP_SECOND_SQRROOT_MISSING -711
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_DLREP_THIRD_SQRROOT_MISSING:
 *
 * Missing representation of the THIRD square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_DLREP_THIRD_SQRROOT_MISSING -712
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_DLREP_FORTH_SQRROOT_MISSING:
 *
 * Missing representation of the FORTH square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_DLREP_FORTH_SQRROOT_MISSING -713
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_DLREP_DELTA_MISSING:
 *
 * Missing representation of the DELTA square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_DLREP_DELTA_MISSING -714
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_FIRST_SQRROOT_MISSING:
 *
 * Missing commitment of the first square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_FIRST_SQRROOT_MISSING -715
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_SECOND_SQRROOT_MISSING:
 *
 * Missing commitment of the SECOND square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_SECOND_SQRROOT_MISSING -716
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_THIRD_SQRROOT_MISSING:
 *
 * Missing commitment of the THIRD square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_THIRD_SQRROOT_MISSING -717
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_FORTH_SQRROOT_MISSING:
 *
 * Missing commitment of the FORTH square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_FORTH_SQRROOT_MISSING -718
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_DELTA_MISSING:
 *
 * Missing commitment of the DELTA square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_DELTA_MISSING -719
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_Q_MISSING:
 *
 * Missing commitment of the DELTA square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_COMMIT_Q_MISSING -720

/**
 * CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_FIRST_SQRROOT_MISSING:
 *
 * Missing responses of the first square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_FIRST_SQRROOT_MISSING -721
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_SECOND_SQRROOT_MISSING:
 *
 * Missing responses of the SECOND square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_SECOND_SQRROOT_MISSING -722
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_THIRD_SQRROOT_MISSING:
 *
 * Missing responses of the THIRD square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_THIRD_SQRROOT_MISSING -723
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_FORTH_SQRROOT_MISSING:
 *
 * Missing responses of the forth square root.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_FORTH_SQRROOT_MISSING -724
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_DELTA_MISSING:
 *
 * Missing responses of DELTA.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_DELTA_MISSING -725
/**
 * CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_RHO_MISSING:
 *
 * Missing response rho.
 */
#define CRYPTIC_PROOF_RANGE_VERIFY_RESPONSES_RHO_MISSING -726
/**
 * CRYPTIC_PROOF_RANGE_VERIFICATION_FAILURE:
 *
 * Range proof not valid.
 */
#define CRYPTIC_PROOF_RANGE_VERIFICATION_FAILURE -727
/**
 * CRYPTIC_PROOF_RANGE_INTERACTIVE_ROUND_1_UNCOMPLETE:
 *
 * Proof range prover round 1 not complete.
 */
#define CRYPTIC_PROOF_RANGE_INTERACTIVE_ROUND_1_UNCOMPLETE -728
/**
 * CRYPTIC_PROOF_RANGE_INTERACTIVE_ROUND_2_NO_CHALLENGE:
 *
 * Challenge missing.
 */
#define CRYPTIC_PROOF_RANGE_INTERACTIVE_ROUND_2_NO_CHALLENGE -729

/**
 * CRYPTIC_HASH_NI_PROOFS_NULL_VALUE:
 *
 * Asked to add a new value before hash computing, but is null.
 */
#define CRYPTIC_HASH_NI_PROOFS_NULL_VALUE -801
/**
 * CRYPTIC_HASH_NI_PROOFS_ERROR_COMPUTATION:
 *
 * Error computing Hash.
 */
#define CRYPTIC_HASH_NI_PROOFS_ERROR_COMPUTATION -802
/**
 * CRYPTIC_ERROR_SSL:
 *
 * Error SSL function.
 */
#define CRYPTIC_ERROR_SSL -900
