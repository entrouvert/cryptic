/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_PROTOCOLS_POK_SCHNORR_H
#define CRYPTIC_PROTOCOLS_POK_SCHNORR_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define CRYPTIC_SCHNORR_CHALLENGE_MIN_SIZE	80

#include <glib.h>
#include <glib-object.h>
#include <openssl/bn.h>

#include "../../export.h"

#define CRYPTIC_TYPE_ZKPKSCHNORR (cryptic_zkpk_schnorr_get_type())
#define CRYPTIC_ZKPKSCHNORR(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), CRYPTIC_TYPE_ZKPKSCHNORR, CrypticZkpkSchnorr))
#define CRYPTIC_ZKPKSCHNORR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), CRYPTIC_TYPE_ZKPKSCHNORR, CrypticZkpkSchnorrClass))
#define CRYPTIC_IS_ZKPKSCHNORR(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRYPTIC_TYPE_ZKPKSCHNORR))
#define CRYPTIC_IS_ZKPKSCHNORR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CRYPTIC_TYPE_ZKPKSCHNORR))
#define CRYPTIC_ZKPKSCHNORR_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CRYPTIC_TYPE_ZKPKSCHNORR, CrypticZkpkSchnorrClass))

typedef struct _CrypticZkpkSchnorr CrypticZkpkSchnorr;
typedef struct _CrypticZkpkSchnorrClass CrypticZkpkSchnorrClass;

/**
 * CrypticZkpkSchnorr:
 * @nb_quantities: Nb of quantities in the DL representation to prove.
 * @bases: base for the DL representation.
 * @dlrep: DL representation to prove.
 * @modulus: modulus of the group.
 * @randoms: Randoms used for the commitment.
 * @commitment: The commitment.
 * @response: responses computed with the challenge.
 *
 * Schnorr zero knowledge proof of knowledge protocol for interactive and non interactive proofs.
 * 
 * The chellange used for interactive proof is a hash for non-interactive proofs.
 * The Fiat-Shamir heuristic introduced a hash function as an oracle.
 *
 */
struct _CrypticZkpkSchnorr {
	GObject parent;

	/*< public >*/
	BIGNUM **randoms;
	BIGNUM *commitment;
	BIGNUM **responses;
	BIGNUM *modulus;
	BIGNUM **bases;
	int nb_quantities;
};

struct _CrypticZkpkSchnorrClass {
	GObjectClass parent_class;
};

CRYPTIC_EXPORT GType cryptic_zkpk_schnorr_get_type(void);

CRYPTIC_EXPORT CrypticZkpkSchnorr* cryptic_zkpk_schnorr_new(BIGNUM **bases,
		int nb_quantities, BIGNUM *modulus);
 

/* Prover */
CRYPTIC_EXPORT int cryptic_zkpk_schnorr_round1(CrypticZkpkSchnorr *shn);

CRYPTIC_EXPORT int cryptic_zkpk_schnorr_round1_one_random_chosen(CrypticZkpkSchnorr *shn,
				 BIGNUM *random, int position);

CRYPTIC_EXPORT int cryptic_zkpk_schnorr_round1_randoms_chosen(CrypticZkpkSchnorr *shn,
				BIGNUM **randoms);

CRYPTIC_EXPORT int cryptic_zkpk_schnorr_round2(CrypticZkpkSchnorr *shn,
				BIGNUM *order, BIGNUM *challenge, BIGNUM **quantities);

CRYPTIC_EXPORT int cryptic_zkpk_schnorr_round2_without_order(CrypticZkpkSchnorr *shn,
				BIGNUM *challenge, BIGNUM **quantities);

/* Verifier */
CRYPTIC_EXPORT int cryptic_zkpk_schnorr_verify_noninteractive_proof(CrypticZkpkSchnorr *shn, BIGNUM *dlrep, BIGNUM *hash, BIGNUM **responses);

CRYPTIC_EXPORT int cryptic_zkpk_schnorr_verify_interactive_proof(CrypticZkpkSchnorr *shn, BIGNUM *dlrep, BIGNUM *commitment, BIGNUM *challenge, BIGNUM **responses);

/* Accessors */
CRYPTIC_EXPORT BIGNUM** cryptic_zkpk_schnorr_get_randoms(CrypticZkpkSchnorr *shn);
CRYPTIC_EXPORT BIGNUM* cryptic_zkpk_schnorr_get_i_random(CrypticZkpkSchnorr *shn, int i);
CRYPTIC_EXPORT BIGNUM* cryptic_zkpk_schnorr_get_commitment(CrypticZkpkSchnorr *shn);
CRYPTIC_EXPORT BIGNUM** cryptic_zkpk_schnorr_get_responses(CrypticZkpkSchnorr *shn);
CRYPTIC_EXPORT BIGNUM* cryptic_zkpk_schnorr_get_i_response(CrypticZkpkSchnorr *shn, int i);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CRYPTIC_PROTOCOLS_POK_SCHNORR_H */
