/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_COMMITMENTS_UTILS_H
#define CRYPTIC_COMMITMENTS_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <openssl/bn.h>

#include "../../export.h"

CRYPTIC_EXPORT BIGNUM* cryptic_get_dlrep(int nb_quantities, BIGNUM **quantities,BIGNUM **bases,BIGNUM *modulus);
CRYPTIC_EXPORT BIGNUM* cryptic_inv_mod(BIGNUM* value, BIGNUM* modulus);
CRYPTIC_EXPORT int cryptic_cmp_bn(BIGNUM* value1, BIGNUM* value2);
CRYPTIC_EXPORT BIGNUM* cryptic_char_to_bn(char* value);
CRYPTIC_EXPORT BIGNUM* cryptic_int_to_bn(int value);

//set_int_to_bn
//set_string_to_bn

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CRYPTIC_PROTOCOLS_POK_SCHNORR_H */
