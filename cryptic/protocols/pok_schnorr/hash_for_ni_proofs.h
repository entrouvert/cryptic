/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_HASH_FOR_NI_PROOFS_H
#define CRYPTIC_HASH_FOR_NI_PROOFS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib.h>
#include <glib-object.h>

#include "../../export.h"

#include "schnorr_zkpk.h"
#include "../proof_range/proof_range_in_qrg.h"

#define CRYPTIC_TYPE_HASHFORNIPROOFS (cryptic_hash_for_ni_proofs_get_type())
#define CRYPTIC_HASHFORNIPROOFS(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), CRYPTIC_TYPE_HASHFORNIPROOFS, CrypticHashForNiProofs))
#define CRYPTIC_HASHFORNIPROOFS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), CRYPTIC_TYPE_HASHFORNIPROOFS, CrypticHashForNiProofsClass))
#define CRYPTIC_IS_HASHFORNIPROOFS(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRYPTIC_TYPE_HASHFORNIPROOFS))
#define CRYPTIC_IS_HASHFORNIPROOFS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CRYPTIC_TYPE_HASHFORNIPROOFS))
#define CRYPTIC_HASHFORNIPROOFS_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CRYPTIC_TYPE_HASHFORNIPROOFS, CrypticHashForNiProofsClass))

typedef struct _CrypticHashForNiProofs CrypticHashForNiProofs;
typedef struct _CrypticHashForNiProofsClass CrypticHashForNiProofsClass;

/**
 * CrypticHashForNiProofs:
 * @size_h: hash size
 *
 * Result in @h_value.
 * Concatenate modulus || bases || dlrep || Commitment and produce a hash.
 *
 */
struct _CrypticHashForNiProofs{
	GObject parent;

	/*< public >*/
	int size_h;
	int nb_modulus;
	int nb_bases;
	int nb_dlreps;
	int nb_commitments;
	BIGNUM *h_value;
	BIGNUM **modulus;
	BIGNUM **bases;
	BIGNUM **dlreps;
	BIGNUM **commitments;
};

struct _CrypticHashForNiProofsClass {
	GObjectClass parent_class;
};


CRYPTIC_EXPORT GType cryptic_hash_for_ni_proofs_get_type(void);

CRYPTIC_EXPORT CrypticHashForNiProofs* cryptic_hash_for_ni_proofs_new(int size_h);

CRYPTIC_EXPORT int cryptic_hash_for_ni_proofs_add_proof(CrypticHashForNiProofs* h, CrypticZkpkSchnorr *shn, BIGNUM *dlrep);
CRYPTIC_EXPORT int cryptic_hash_for_ni_proofs_add_proofrange_prover(CrypticHashForNiProofs* h, CrypticProofrangeQrg* pr);
CRYPTIC_EXPORT int cryptic_hash_for_ni_proofs_add_proofrange_verifier(CrypticHashForNiProofs* h, CrypticProofrangeQrg* pr, BIGNUM **dlreps);

CRYPTIC_EXPORT int cryptic_hash_for_ni_proofs_add_modulus(CrypticHashForNiProofs* h, BIGNUM* modulus);
CRYPTIC_EXPORT int cryptic_hash_for_ni_proofs_add_base(CrypticHashForNiProofs* h, BIGNUM* base);
CRYPTIC_EXPORT int cryptic_hash_for_ni_proofs_add_dlrep(CrypticHashForNiProofs* h, BIGNUM* dlrep);
CRYPTIC_EXPORT int cryptic_hash_for_ni_proofs_add_commitment(CrypticHashForNiProofs* h, BIGNUM* commitment);

CRYPTIC_EXPORT int cryptic_hash_for_ni_proofs_compute_hash(CrypticHashForNiProofs* h);

/* Accessors */
CRYPTIC_EXPORT BIGNUM* cryptic_hash_for_ni_proofs_get_hash(CrypticHashForNiProofs* h);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CRYPTIC_HASH_FOR_NI_PROOFS_H */
