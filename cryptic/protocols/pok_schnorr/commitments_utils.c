/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <openssl/bn.h>

#include <string.h>

#include "../../errors.h"
#include "../../utils.h"

#include "commitments_utils.h"

BIGNUM*
cryptic_get_dlrep(int nb_quantities, BIGNUM **quantities, BIGNUM **bases, BIGNUM *modulus)
{
	int rc = CRYPTIC_ERROR_UNDEFINED;

	int j;
	BIGNUM *ret = NULL, *tmp = NULL;
	BN_CTX *ctx = NULL;

	goto_cleanup_if_fail(modulus != NULL && quantities != NULL && bases != NULL);

	for(j=0; j<nb_quantities; j++){
		goto_cleanup_if_fail(bases[j] != NULL);
		goto_cleanup_if_fail(quantities[j] != NULL);
	}

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ret = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(tmp = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(ret,1) == 1);

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ctx = BN_CTX_new());

	for(j=0; j<nb_quantities; j++){
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_mod_exp(tmp,bases[j],quantities[j],modulus,ctx) == 1);
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_mod_mul(ret,ret,tmp,modulus,ctx) == 1);
	}

	rc = CRYPTIC_NO_ERROR;
cleanup:
	cryptic_release_ctx(ctx);
	cryptic_release_bn(tmp);
	if(rc != CRYPTIC_NO_ERROR){
		cryptic_release_bn(ret);
		return NULL;
	}
	return ret;
}

BIGNUM* cryptic_inv_mod(BIGNUM* value, BIGNUM* modulus)
{
	int rc = CRYPTIC_ERROR_UNDEFINED;

	if(!modulus || !value) return NULL;

	BIGNUM *ret = NULL, *tmp = NULL;
	BN_CTX *ctx = NULL;

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ret = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(tmp = BN_new());

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ctx = BN_CTX_new());

	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_gcd(tmp, modulus, value, ctx) == 1);
	goto_cleanup_if_fail(BN_ucmp(tmp, BN_value_one()) == 0); /* Not inversible */

	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_mod_inverse(ret,value,modulus,ctx));

	rc = CRYPTIC_NO_ERROR;
cleanup:
	cryptic_release_ctx(ctx);
	cryptic_release_bn(tmp);
	if(rc != CRYPTIC_NO_ERROR){
		cryptic_release_bn(ret);
		return NULL;
	}
	return ret;
}

int cryptic_cmp_bn(BIGNUM* value1, BIGNUM* value2)
{
	if(!value1 || !value2) return -1;
	if(BN_ucmp(value1,value2) == 0)
		return 1;
	else
		return 0;
}

BIGNUM*
cryptic_char_to_bn(char* value)
{
	if(!value || value[0]==0) return NULL;
	BIGNUM *ret = NULL;
	if(!(ret = BN_new())) return NULL;
	BN_bin2bn((const unsigned char *) value,strlen(value),ret);
	return ret;
}

BIGNUM*
cryptic_int_to_bn(int value)
{
	int rc = CRYPTIC_ERROR_UNDEFINED;

	BIGNUM *ret = NULL;
	goto_cleanup_if_fail_with_rc_with_warning_openssl(ret = BN_new());

	if(value < 0){
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(ret,abs(value)) == 1);
		BN_set_negative(ret,1);
	}else{
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(ret,value) == 1);
	}

	rc = CRYPTIC_NO_ERROR;
cleanup:
	if(rc != CRYPTIC_NO_ERROR){
		cryptic_release_bn(ret);
		return NULL;
	}
	return ret;
}

