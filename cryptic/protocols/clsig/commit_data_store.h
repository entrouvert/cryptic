/* CRYPTIC -- Certificates tools
 * Copyright (C) 2010 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_COMMITDATASTORE_H
#define CRYPTIC_COMMITDATASTORE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*#ifndef CRYPTIC_SCHNORR_CHALLENGE_MIN_SIZE
#define CRYPTIC_SCHNORR_CHALLENGE_MIN_SIZE	80
#endif*/ 

#include <glib.h>
#include <glib-object.h>
#include <openssl/bn.h>

#include "../../export.h"

#define CRYPTIC_TYPE_COMMITDATASTORE (cryptic_commit_data_store_get_type())
#define CRYPTIC_COMMITDATASTORE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), CRYPTIC_TYPE_COMMITDATASTORE, CrypticCommitDataStore))
#define CRYPTIC_COMMITDATASTORE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), CRYPTIC_TYPE_COMMITDATASTORE, CrypticCommitDataStoreClass))
#define CRYPTIC_IS_COMMITDATASTORE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRYPTIC_TYPE_COMMITDATASTORE))
#define CRYPTIC_IS_COMMITDATASTORE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CRYPTIC_TYPE_COMMITDATASTORE))
#define CRYPTIC_COMMITDATASTORE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CRYPTIC_TYPE_COMMITDATASTORE, CrypticCommitDataStoreClass))

typedef struct _CrypticCommitDataStore CrypticCommitDataStore;
typedef struct _CrypticCommitDataStoreClass CrypticCommitDataStoreClass;

/**
 * CrypticCommitDataStore:
 * @dlrep: DL representation to prove.
 * @vprime: S exponent for commit (roughtly a blind value).
 *
 * Store to avoid multiple return parameters not easily manageable with bindings.
 *
 */
struct _CrypticCommitDataStore {
	GObject parent;

	/*< public >*/
	BIGNUM *vprime;
	BIGNUM *dlrep;

};

struct _CrypticCommitDataStoreClass {
	GObjectClass parent_class;
};

CRYPTIC_EXPORT GType cryptic_commit_data_store_get_type(void);

CRYPTIC_EXPORT CrypticCommitDataStore* cryptic_commit_data_store_new();
 
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CRYPTIC_COMMITDATASTORE_H */
