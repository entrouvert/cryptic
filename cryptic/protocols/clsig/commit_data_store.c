/* X23 -- Certificates tools
 * Copyright (C) 2010 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <glib.h>
#include <glib-object.h>
#include <stdio.h>
#include <string.h>

#include "../../errors.h"
#include "../../utils.h"

#include "commit_data_store.h"

/*****************************************************************************/
/* private methods                                                           */
/*****************************************************************************/

static GObjectClass *parent_class = NULL;

/*****************************************************************************/
/* overridden parent class methods                                           */
/*****************************************************************************/

static void
dispose(GObject *object)
{
	CrypticCommitDataStore *pdc = CRYPTIC_COMMITDATASTORE(object);

	cryptic_release_bn(pdc->dlrep);
	cryptic_release_bn(pdc->vprime);

	G_OBJECT_CLASS(parent_class)->dispose(G_OBJECT(pdc));
}

/*****************************************************************************/
/* instance and class init functions                                         */
/*****************************************************************************/

static void
instance_init(CrypticCommitDataStore *pdc)
{
	pdc->dlrep = NULL;
	pdc->vprime = NULL;
}

static void
class_init(CrypticCommitDataStoreClass *klass)
{
	parent_class = g_type_class_peek_parent(klass);
	G_OBJECT_CLASS(klass)->dispose = dispose;
}

/*****************************************************************************/
/* public methods                                                            */
/*****************************************************************************/

GType
cryptic_commit_data_store_get_type()
{
	static GType this_type = 0;
	if (!this_type) {
		static const GTypeInfo this_info = {
			sizeof (CrypticCommitDataStoreClass),
			NULL,
			NULL,
			(GClassInitFunc) class_init,
			NULL,
			NULL,
			sizeof(CrypticCommitDataStore),
			0,
			(GInstanceInitFunc) instance_init,
			NULL
		};
		this_type = g_type_register_static(G_TYPE_OBJECT,
				"CrypticCommitDataStore", &this_info, 0);
	}
	return this_type;
}

/**
 * cryptic_commit_data_store_new
 *
 * Creates a new #CrypticCommitDataStore.
 *
 * Return value: a newly created #CrypticCommitDataStore object; or NULL if an error
 *     occured
 **/
CrypticCommitDataStore*
cryptic_commit_data_store_new()
{
	CrypticCommitDataStore *pdc;
	pdc = g_object_new(CRYPTIC_TYPE_COMMITDATASTORE, NULL);
	return pdc;
}
