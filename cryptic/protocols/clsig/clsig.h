/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CLSIG_H
#define CLSIG_H


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define CRYPTIC_CLSIG_MODULUS_SIZE 				2048 	//ln
#define CRYPTIC_CLSIG_COMMITMENT_GROUP_MODULUS_SIZE 		1632	//lRHO
#define CRYPTIC_CLSIG_COMMITMENT_GROUP_PRIME_ORDER_SIZE 	256	//lrho
#define CRYPTIC_CLSIG_QUANTITIES_SIZE 				256	//lm
#define CRYPTIC_CLSIG_EXPONENT_VALUES				596	//le
#define CRYPTIC_CLSIG_EXPONENT_INTERVAL				120	//lei
#define CRYPTIC_CLSIG_BLIND_VALUES				2723	//lv
#define CRYPTIC_CLSIG_CHALLENGE_SIZE				256	//challenge: lH for non interactive proofs - lc for interactive proofs
#define CRYPTIC_CLSIG_ZK_SEC_PARAM				80	//l0
#define CRYPTIC_CLSIG_SEC_PARAM					160	//lk
#define CRYPTIC_CLSIG_SEC_PARAM_CRED_SYS			80	//lr

#define CRYPTIC_CLSIG_TEST_MODULUS_SIZE 			1024 	//ln
#define CRYPTIC_CLSIG_TEST_COMMITMENT_GROUP_MODULUS_SIZE 	816	//lRHO
#define CRYPTIC_CLSIG_TEST_COMMITMENT_GROUP_PRIME_ORDER_SIZE 	128	//lrho
#define CRYPTIC_CLSIG_TEST_QUANTITIES_SIZE 			128	//lm
#define CRYPTIC_CLSIG_TEST_EXPONENT_VALUES			298	//le
#define CRYPTIC_CLSIG_TEST_EXPONENT_INTERVAL			60	//lei
#define CRYPTIC_CLSIG_TEST_BLIND_VALUES				1461	//lv
#define CRYPTIC_CLSIG_TEST_CHALLENGE_SIZE			128	//challenge: lH for non interactive proofs - lc for interactive proofs
#define CRYPTIC_CLSIG_TEST_ZK_SEC_PARAM				40	//l0
#define CRYPTIC_CLSIG_TEST_SEC_PARAM				80	//lk
#define CRYPTIC_CLSIG_TEST_SEC_PARAM_CRED_SYS			40	//lr

		/** Constraints
		* lH (hash) for non interactive proofs - lc (chalenge) for interactive proofs
		* 1- le > l0 + lH + max( lm+4 , lei+2 )
		* 2- lv > ln + l0 + lH + max ( lm+lr+3 , l0+2 )
		* 3- lH >= lk
		* 4- lH < le (cf. 1)
		* 5- lei < le - l0 - lH - 3 (computed after checking 1)
		* 6- lm = lH (The larger the better and lm <= lH thus we only care of lm) 
		* 7- lrand = ln + l0
		*/

#include <glib.h>
#include <glib-object.h>
#include <openssl/bn.h>

#include "../../export.h"

#include "../../maths/quadratic_residues_group.h"
#include "../pok_schnorr/schnorr_zkpk.h"
#include "commit_data_store.h"

#define CRYPTIC_TYPE_CLSIG (cryptic_clsig_get_type())
#define CRYPTIC_CLSIG(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), CRYPTIC_TYPE_CLSIG, CrypticClsig))
#define CRYPTIC_CLSIG_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), CRYPTIC_TYPE_CLSIG, CrypticClsigClass))
#define CRYPTIC_IS_CLSIG(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRYPTIC_TYPE_CLSIG))
#define CRYPTIC_IS_CLSIG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CRYPTIC_TYPE_CLSIG))
#define CRYPTIC_CLSIG_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CRYPTIC_TYPE_CLSIG, CrypticClsigClass))

typedef struct _CrypticClsig CrypticClsig;
typedef struct _CrypticClsigClass CrypticClsigClass;

/**
 * CrypticClsig:
 * @lg_quantities: lm - we only care of lm
 * @lg_modulus: ln
 * @lg_exponent: le
 * @interval_exponent: lei
 * @lg_blind: lv
 * @lg_randomize: lrand
 * @lg_sec_param: lk
 * @lg_zk_sec_param: l0
 * @lg_clsig_sec_param: lr
 * @nb_quantities:
 * @nb_bases: number of bases not including S and Z
 * @modulus: n
 * @S: base to blind dlrep
 * @Z: dlrep to prove to prove a certificate
 * @bases: bases to rep quantities
 * @quantities: quantities signed
 * @dlrep: quantities representes in DL
 * @A: Signature value
 * @v: quantity to blind the representation of quantities
 * @e: RSA exponent
 * @d: RSA private exponent
 * @r: random used to randomize the signature
 * @A_rand: Signature randomized
 * @v_rand: quantity used to blind the representation after randomization
 * @e_corrected: the RSA exponent corrected
 * @qrg: quadratic residues group
 *
 * Camenisch-Lysyanskaya Signature.
 *
 */
struct _CrypticClsig{
	GObject parent;

	/*< public >*/
	int lg_quantities;		/* lm */
	int lg_modulus;			/* ln */
	int lg_exponent; 		/* le */
	int interval_exponent;		/* lei */
	int lg_blind;			/* lv */
	int lg_randomize; 		/* lrand */
	int lg_sec_param;		/* lk */
	int lg_zk_sec_param;		/* l0 */
	int lg_clsig_sec_param;		/* lr */
	int nb_quantities;
	int nb_bases;			/* number of bases not including S and Z*/

	BIGNUM *modulus;		/* n */
	BIGNUM *S;			/* base to blind dlrep */
	BIGNUM *Z;			/* dlrep to prove to prove a certificate */
	BIGNUM **bases;			/* bases to rep quantities */
	BIGNUM **quantities;		/* quantities signed */
	BIGNUM *dlrep;			/* quantities representes in DL */
	BIGNUM *A;			/* Signature value */
	BIGNUM *v;			/* quantity to blind the representation of quantities */
	BIGNUM *e;			/* RSA exponent */
	BIGNUM *d;			/* RSA private exponent */
	BIGNUM *r;			/* random used to randomize the signature */
	BIGNUM *A_rand;			/* Signature randomized */
	BIGNUM *v_rand;			/* quantity used to blind the representation after randomization */
	BIGNUM *e_corrected;

	CrypticQRG *qrg;

	/*< private >*/
	int sigrandomized;
	int sigverified;
	int sigloaded;
	int init;
};

struct _CrypticClsigClass {
	GObjectClass parent_class;
};

CRYPTIC_EXPORT GType cryptic_clsig_get_type(void);

CRYPTIC_EXPORT CrypticClsig* cryptic_clsig_new(int lg_modulus, int lg_quantities, int lg_exponent, int lg_sec_param, int lg_zk_sec_param, int lg_clsig_sec_param, int nb_bases);

/* Issuer: 1a */
//CRYPTIC_EXPORT int cryptic_clsig_generate_parameters(CrypticClsig *clsig, BN_GENCB *cb);
CRYPTIC_EXPORT int cryptic_clsig_generate_parameters(CrypticClsig *clsig);
//CRYPTIC_EXPORT int cryptic_clsig_find_rsa_param(CrypticClsig *clsig, int lg_exponent, BN_GENCB *cb);
CRYPTIC_EXPORT int cryptic_clsig_find_rsa_param(CrypticClsig *clsig, int lg_exponent);
//CRYPTIC_EXPORT int cryptic_clsig_find_rsa_param_from_pubexp(CrypticClsig *clsig, BIGNUM *e, BN_GENCB *cb);
CRYPTIC_EXPORT int cryptic_clsig_find_rsa_param_from_pubexp(CrypticClsig *clsig, BIGNUM *e);

CRYPTIC_EXPORT int cryptic_clsig_add_n_bases(CrypticClsig *clsig, int nb);

/* Issuer: 1b */
CRYPTIC_EXPORT CrypticClsig* cryptic_clsig_new_load_parameters_issuer(BIGNUM *p,
					BIGNUM *Z,
					BIGNUM *S,
					int nb_bases,
					BIGNUM **bases,
					int lg_quantities,
					int lg_exponent,
					BIGNUM *modulus,
					int lg_sec_param,
					int lg_zk_sec_param,
					int lg_clsig_sec_param);
//					int lg_clsig_sec_param,
//					BN_GENCB *cb);

/* Prover and Verifier: 1 */
CRYPTIC_EXPORT CrypticClsig* cryptic_clsig_new_load_public_parameters(BIGNUM *Z,
					BIGNUM *S,
					int nb_bases,
					BIGNUM **bases,
					int lg_quantities,
					int lg_exponent,
					BIGNUM *modulus,
					int lg_sec_param,
					int lg_zk_sec_param,
					int lg_clsig_sec_param);

/* Issuer: 2 */
/* 2.1 */
/* Helper functions to represent quantities */
CRYPTIC_EXPORT int cryptic_clsig_compute_dlrep_with_random_quantities(CrypticClsig *clsig, int nb_quantities);
CRYPTIC_EXPORT int cryptic_clsig_compute_dlrep_by_index(CrypticClsig *clsig, BIGNUM **quantities, int *index, int nb_quantities);
CRYPTIC_EXPORT int cryptic_clsig_compute_dlrep_bulk_from_offset(CrypticClsig *clsig, BIGNUM **quantities, int offset, int nb_quantities);
/* 2.2 */
CRYPTIC_EXPORT int cryptic_clsig_sign(CrypticClsig *clsig);
CRYPTIC_EXPORT int cryptic_clsig_sign_with_committed_value(CrypticClsig *clsig, BIGNUM *commitment);

/* Prover: 3 */
CRYPTIC_EXPORT int cryptic_clsig_compute_committed_value(CrypticClsig *clsig,
				CrypticCommitDataStore *pdc, 
				BIGNUM** bases, 
				BIGNUM** quantities, int nb_quantities);
CRYPTIC_EXPORT int cryptic_clsig_compute_committed_value_with_index(CrypticClsig *clsig,
				CrypticCommitDataStore *pdc, 
				int *index, 
				BIGNUM** quantities, int nb_quantities);
CRYPTIC_EXPORT int cryptic_clsig_load_certificate(CrypticClsig *clsig,
				BIGNUM *A,
				BIGNUM *e,
				BIGNUM *v,
				BIGNUM **quantities, int nb_quantities,
				int sig_checking);
CRYPTIC_EXPORT int cryptic_clsig_load_certificate_with_index(CrypticClsig *clsig,
				BIGNUM *A,
				BIGNUM *e,
				BIGNUM *v,
				BIGNUM **quantities,
				int nb_quantities,
				int* index,
				int sig_checking);
CRYPTIC_EXPORT int cryptic_clsig_load_certificate_with_committed_value(CrypticClsig *clsig,
				BIGNUM *A,
				BIGNUM *e,
				BIGNUM *v,
				BIGNUM **quantities, int nb_quantities,
				BIGNUM **quantitiesC, int nb_quantitiesC,
				BIGNUM *commitment, BIGNUM *vprime);
CRYPTIC_EXPORT int cryptic_clsig_load_certificate_with_index_with_committed_value(CrypticClsig *clsig,
				BIGNUM *A,
				BIGNUM *e,
				BIGNUM *v,
				BIGNUM **quantities, int nb_quantities,
				BIGNUM **quantitiesC, int nb_quantitiesC,
				BIGNUM *commitment, BIGNUM *vprime,
				int *index);
CRYPTIC_EXPORT int cryptic_clsig_randomize_signature(CrypticClsig *clsig);

CRYPTIC_EXPORT int cryptic_clsig_run_zkpk_schnorr_round1(CrypticClsig *clsig, CrypticZkpkSchnorr *shn);

/* Issuer and Prover and Verifier */
CRYPTIC_EXPORT int cryptic_clsig_verify_rsa_param(CrypticClsig *clsig);
CRYPTIC_EXPORT int cryptic_clsig_verify_signature_not_randomized(CrypticClsig *clsig);
CRYPTIC_EXPORT int cryptic_clsig_verify_signature_randomized(CrypticClsig *clsig);

CRYPTIC_EXPORT BIGNUM* cryptic_clsig_correct_dlrep_before_proving(CrypticClsig *clsig, BIGNUM* dlrep);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_build_dlrep_before_proving(CrypticClsig *clsig, int* index_quantities, int* index_bases, int nb_quantities);

/* Helper functions to extract parameters */
CRYPTIC_EXPORT int cryptic_clsig_copy_generators_in_tab(BIGNUM **ret, CrypticClsig *clsig);
CRYPTIC_EXPORT int cryptic_clsig_copy_generators_by_index_in_tab(BIGNUM **ret, CrypticClsig *clsig, int *index, int nb_gen);
CRYPTIC_EXPORT int cryptic_clsig_copy_generators_bulk_from_offset_in_tab(BIGNUM **ret, CrypticClsig *clsig, int offset, int nb_gen);
CRYPTIC_EXPORT int cryptic_clsig_copy_quantities_in_tab(BIGNUM **ret, CrypticClsig *clsig);

/* Accessors */
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_private_composite(CrypticClsig *clsig); /* p */
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_modulus(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_order(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_exponent(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_exponent_corrected(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_private_exponent(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_signature(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_randomized_signature(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_random_for_randomized_signature(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_DL_representation(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_blind(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_random_blind(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_S(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_Z(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM** cryptic_clsig_get_bases(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM** cryptic_clsig_get_quantities(CrypticClsig *clsig);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_i_base(CrypticClsig *clsig, int i);
CRYPTIC_EXPORT BIGNUM* cryptic_clsig_get_i_quantity(CrypticClsig *clsig, int i);
int cryptic_clsig_get_nb_bases(CrypticClsig *clsig);
int cryptic_clsig_get_nb_quantities(CrypticClsig *clsig);
int cryptic_clsig_get_lg_quantities(CrypticClsig *clsig);
int cryptic_clsig_get_lg_modulus(CrypticClsig *clsig);
int cryptic_clsig_get_lg_blind(CrypticClsig *clsig);
int cryptic_clsig_get_lg_randomize(CrypticClsig *clsig);
int cryptic_clsig_get_lg_exponent(CrypticClsig *clsig);
int cryptic_clsig_get_interval_exponent(CrypticClsig *clsig);
int cryptic_clsig_get_lg_sec_param(CrypticClsig *clsig);
int cryptic_clsig_get_lg_zk_sec_param(CrypticClsig *clsig);
int cryptic_clsig_get_lg_clsig_sec_param(CrypticClsig *clsig);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* CLSIG_H */
