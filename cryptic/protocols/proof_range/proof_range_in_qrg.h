/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_PROOF_RANGE_H
#define CRYPTIC_PROOF_RANGE_H


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define CRYPTIC_PROOF_RANGE_GT 		1
#define CRYPTIC_PROOF_RANGE_GTE 	2
#define CRYPTIC_PROOF_RANGE_LT 		3
#define CRYPTIC_PROOF_RANGE_LTE 	4

#define CRYPTIC_PROOF_RANGE_ZK_SEC_PARAM			80	//l0
#define CRYPTIC_PROOF_RANGE_SEC_PARAM				160	//lk
#define CRYPTIC_PROOF_RANGE_CHALLENGE_MIN_SIZE			160	//lc

#define CRYPTIC_PROOF_RANGE_TEST_ZK_SEC_PARAM			40	//l0
#define CRYPTIC_PROOF_RANGE_TEST_SEC_PARAM			80	//lk
#define CRYPTIC_PROOF_RANGE_TEST_CHALLENGE_MIN_SIZE		80	//lc

#include <glib.h>
#include <glib-object.h>
#include <openssl/bn.h>

#include "../../export.h"

#include "../pok_schnorr/schnorr_zkpk.h"
#include "../../maths/decompose_integer.h"

#define CRYPTIC_TYPE_PROOFRANGEQRG (cryptic_proofrange_qrg_get_type())
#define CRYPTIC_PROOFRANGEQRG(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), CRYPTIC_TYPE_PROOFRANGEQRG, CrypticProofrangeQrg))
#define CRYPTIC_PROOFRANGEQRG_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), CRYPTIC_TYPE_PROOFRANGEQRG, CrypticProofrangeQrgClass))
#define CRYPTIC_IS_PROOFRANGE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRYPTIC_TYPE_PROOFRANGEQRG))
#define CRYPTIC_IS_PROOFRANGE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CRYPTIC_TYPE_PROOFRANGEQRG))
#define CRYPTIC_PROOFRANGEQRG_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CRYPTIC_TYPE_PROOFRANGEQRG, CrypticProofrangeQrgClass))

typedef struct _CrypticProofrangeQrg CrypticProofrangeQrg;
typedef struct _CrypticProofrangeQrgClass CrypticProofrangeQrgClass;

/**
 * CrypticProofrangeQrg:
 * @rel: inequality to prove
 * @m: quantity on which the range proof is led
 * @b: quantity of comparison
 * @delta: Absolute value of the difference of b - m (-1)
 * @di: decomposition in squares of delta
 * @randDLRepDelta: <describe>
 * @dLRepDelta: dlrep Delta (Td)
 * @tabRandDLRepSqrRoot: random to dlrep the square roots of Delta
 * @tabDLRepSqrRoot: dlrep of the square roots of Delta (Ti)
 * @randQ: <describe>
 * @Q: <describe>
 * @challenge: <describe>
 * @tabBases: e.g. S and Z for CLSIG
 * @responserho: <describe>
 * @dlreps: T1, T2, T3, T4, T5, Tdelta
 * @commitments: t1, t2, t3, t4, t5, tdelta, tQ
 * @responses: su1, zu1, su2, zu2, su3, zu3, su4, zu4, srho
 * @modulus: <describe>
 * @pokSqrRoot1: Proof of T1 = -/+ Z^u1.S^randDLRepSqrRoot[0]
 * @pokSqrRoot2: Proof of T2 = -/+ Z^u2.S^randDLRepSqrRoot[1]
 * @pokSqrRoot3: Proof of T3 = -/+ Z^u3.S^randDLRepSqrRoot[2]
 * @pokSqrRoot4:Proof of T3 = -/+ Z^u4.S^randDLRepSqrRoot[3]
 * @pokDelta: Proof of Td = T1^u1.T2^u2.T3^u3.T4^u4.S^(randDLRepDelta - (u1r1 + u2r2 + u3r3 + u4r4))
 *
 * This class is used to lead a proof of an inequality statement on a value in a DL representation.
 * The quantity m can be proved >, >=, < or <= to b.
 *
 * WARNING: This proof is for commitments and proofs led in a composite group.
 * Consists in prooving that a value is positive (e.g. delta = b-m to prove that m < b).
 * The prover does not know the order and thus cannot make compute the proof for negative values.
 * Boudot, Efficient proofs that a committed number lies in an interval, 2000, divide the proof into two positivity proofs.
 * Decomposition in four squares for this purpose introduced by Lipmaa:
 * Helger Lipmaa, Statistical zero-knowledge proofs from diophantine equations, 2001.
 * Decomposition is due to Lagrange's four square theorem (1770) also know as Bachet's conjecture.
 * Algorithm of decomposition due to Rabin and Shalit, Randomized algorithms in number theory, 1986.
 *
 */
struct _CrypticProofrangeQrg{
	GObject parent;

	/*< public >*/
	int rel;

	int lg_sec_param;		/* lk */
	int lg_zk_sec_param;		/* l0 */
	int lg_challenge;		/* lc */

	BIGNUM *m;
	BIGNUM *b;
	BIGNUM *delta;
	BIGNUM *randDLRepDelta;
	BIGNUM **tabRandDLRepSqrRoot;
	BIGNUM *randQ;
	BIGNUM *Q;
	BIGNUM *challenge;
	BIGNUM **tabBases;
	BIGNUM *responserho;
	BIGNUM **dlreps;
	BIGNUM **commitments;
	BIGNUM **responses;
	BIGNUM *modulus;

	CrypticZkpkSchnorr *pokSqrRoot1;
	CrypticZkpkSchnorr *pokSqrRoot2;
	CrypticZkpkSchnorr *pokSqrRoot3;
	CrypticZkpkSchnorr *pokSqrRoot4;
	CrypticZkpkSchnorr *pokDelta;

	CrypticDecomposeInteger *di;
};

struct _CrypticProofrangeQrgClass {
	GObjectClass parent_class;
};


CRYPTIC_EXPORT GType cryptic_proofrange_qrg_get_type(void);

CRYPTIC_EXPORT CrypticProofrangeQrg* cryptic_proofrange_qrg_new(BIGNUM *base1, BIGNUM *base2, BIGNUM *modulus);


/* Prover */
CRYPTIC_EXPORT int cryptic_proofrange_qrg_round1(CrypticProofrangeQrg *pr,
			int rel,
			BIGNUM *m,
			BIGNUM *b,
			BIGNUM *random_m,
			int lg_sec_param,
			int lg_zk_sec_param,
			int lg_challenge);

CRYPTIC_EXPORT int cryptic_proofrange_qrg_round2(CrypticProofrangeQrg *pr, BIGNUM *challenge);


/* Verifier */
CRYPTIC_EXPORT int cryptic_proofrange_qrg_verify_interactive_proof(CrypticProofrangeQrg *pr,
			int rel,
			BIGNUM *b,
			BIGNUM **dlreps,
			BIGNUM **commitments,
			BIGNUM *challenge,
			BIGNUM **responses);

CRYPTIC_EXPORT int cryptic_proofrange_qrg_verify_noninteractive_proof(CrypticProofrangeQrg *pr,
			int rel,
			BIGNUM *b,
			BIGNUM **dlreps,
			BIGNUM *hash,
			BIGNUM **responses);

/* Accessors */
CRYPTIC_EXPORT BIGNUM* cryptic_proofrange_qrg_get_modulus(CrypticProofrangeQrg *pr);
CRYPTIC_EXPORT BIGNUM** cryptic_proofrange_qrg_get_dlreps(CrypticProofrangeQrg *pr);
CRYPTIC_EXPORT BIGNUM** cryptic_proofrange_qrg_get_commitments(CrypticProofrangeQrg *pr);
CRYPTIC_EXPORT BIGNUM** cryptic_proofrange_qrg_get_responses(CrypticProofrangeQrg *pr);
CRYPTIC_EXPORT BIGNUM** cryptic_proofrange_qrg_get_tabRandDLRepSqrRoot(CrypticProofrangeQrg *pr);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CRYPTIC_PROOF_RANGE_H */
