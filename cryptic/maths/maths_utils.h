/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_MATHS_UTILS_H
#define CRYPTIC_MATHS_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <openssl/bn.h>

#include "../export.h"

CRYPTIC_EXPORT int cryptic_find_random(BIGNUM *ret, int size);
CRYPTIC_EXPORT int cryptic_find_random_with_range_value(BIGNUM *ret, BIGNUM *value);

CRYPTIC_EXPORT BIGNUM* cryptic_ret_random(int size);
CRYPTIC_EXPORT BIGNUM* cryptic_ret_random_with_range_value(BIGNUM *value);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CRYPTIC_MATHS_UTILS_H */
