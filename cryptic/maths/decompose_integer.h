/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_MATHS_DECOMPOSE_INTEGER_H
#define CRYPTIC_MATHS_DECOMPOSE_INTEGER_H


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <glib.h>
#include <glib-object.h>
#include <openssl/bn.h>

#include "../export.h"


#define CRYPTIC_TYPE_DECOMPOSEINTEGER (cryptic_decompose_integer_get_type())
#define CRYPTIC_DECOMPOSEINTEGER(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), CRYPTIC_TYPE_DECOMPOSEINTEGER, CrypticDecomposeInteger))
#define CRYPTIC_DECOMPOSEINTEGER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), CRYPTIC_TYPE_DECOMPOSEINTEGER, CrypticDecomposeIntegerClass))
#define CRYPTIC_IS_DECOMPOSEINTEGER(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRYPTIC_TYPE_DECOMPOSEINTEGER))
#define CRYPTIC_IS_DECOMPOSEINTEGER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CRYPTIC_TYPE_DECOMPOSEINTEGER))
#define CRYPTIC_DECOMPOSEINTEGER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CRYPTIC_TYPE_DECOMPOSEINTEGER, CrypticDecomposeIntegerClass))

typedef struct _CrypticDecomposeInteger CrypticDecomposeInteger;
typedef struct _CrypticDecomposeIntegerClass CrypticDecomposeIntegerClass;

/**
 * CrypticDecomposeInteger:
 * @a: First square.
 * @b: Second square.
 * @c: Third square.
 * @d: Forth square.
 *
 * Decompose an integer in four squares.
 *
 */
struct _CrypticDecomposeInteger{
	GObject parent;

	/*< public >*/
	BIGNUM *a;
	BIGNUM *b;
	BIGNUM *c;
	BIGNUM *d;

	/*< private >*/
	BIGNUM *ONE;
	BIGNUM *TWO;
	BIGNUM *THREE;
	BIGNUM *FOUR;
	BIGNUM *FIVE;
	BIGNUM *SEVEN;
	BIGNUM *EIGHT;

};

struct _CrypticDecomposeIntegerClass {
	GObjectClass parent_class;
};

CRYPTIC_EXPORT GType cryptic_decompose_integer_get_type(void);

CRYPTIC_EXPORT CrypticDecomposeInteger* cryptic_decompose_integer_new(BIGNUM *numToDecompose);
CRYPTIC_EXPORT int cryptic_decompose_integer_sum_two_squares(CrypticDecomposeInteger *di, BIGNUM *numToDecompose);
CRYPTIC_EXPORT int cryptic_decompose_integer_newton_iteration(CrypticDecomposeInteger *di, BIGNUM *square, BIGNUM *root);
CRYPTIC_EXPORT int cryptic_decompose_integer_square_root(CrypticDecomposeInteger *di, BIGNUM *square, BIGNUM *root);
CRYPTIC_EXPORT int cryptic_decompose_integer_back_reduction(CrypticDecomposeInteger *di, int reduction);
CRYPTIC_EXPORT int cryptic_decompose_integer_verify_decomposition(CrypticDecomposeInteger *di, BIGNUM *numToDecompose);

/* Accessors */
BIGNUM* cryptic_getSqrRoot1(CrypticDecomposeInteger *di);
BIGNUM* cryptic_getSqrRoot2(CrypticDecomposeInteger *di);
BIGNUM* cryptic_getSqrRoot3(CrypticDecomposeInteger *di);
BIGNUM* cryptic_getSqrRoot4(CrypticDecomposeInteger *di);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* CRYPTIC_MATHS_DECOMPOSE_INTEGER_H */
