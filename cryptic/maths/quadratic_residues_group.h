/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_MATHS_QR_GROUP_H
#define CRYPTIC_MATHS_QR_GROUP_H


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define CRYPTIC_MATHS_QR_GROUP_MODULUS_SIZE 				2048
#define CRYPTIC_MATHS_QR_GROUP_TEST_MODULUS_SIZE 			1024

#include <glib.h>
#include <glib-object.h>
#include <openssl/bn.h>
#include <openssl/bio.h>

#include "../export.h"

#define CRYPTIC_TYPE_QRGROUP (cryptic_qrg_get_type())
#define CRYPTIC_QRGROUP(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), CRYPTIC_TYPE_QRGROUP, CrypticQRG))
#define CRYPTIC_QRGROUP_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), CRYPTIC_TYPE_QRGROUP, CrypticQRGClass))
#define CRYPTIC_IS_QRGROUP(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRYPTIC_TYPE_QRGROUP))
#define CRYPTIC_IS_QRGROUP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CRYPTIC_TYPE_QRGROUP))
#define CRYPTIC_QRGROUP_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CRYPTIC_TYPE_QRGROUP, CrypticQRGClass))

typedef struct _CrypticQRG CrypticQRG;
typedef struct _CrypticQRGClass CrypticQRGClass;

/**
 * CrypticZkpkInteractiveSchnorr:
 * @p: Safe prime.
 * @q: Safe prime.
 * @pp: pp = (p-1)/2
 * @qq: qq = (q-1)/2
 * @n: modulus (p*q).
 * @order: Order of the group (pp*qq).
 * @phi: Euler phi(n) = (4*pp*qq).
 *
 * Group of quadratic residues.
 *
 */
struct _CrypticQRG{
	GObject parent;

	/*< public >*/
	BIGNUM *p;
	BIGNUM *q;
	BIGNUM *pp;
	BIGNUM *qq;
	BIGNUM *n;
	BIGNUM *order;
	BIGNUM *phi;
	BIGNUM *base;
	BIGNUM *two;
	int lg_modulus;
};

struct _CrypticQRGClass {
	GObjectClass parent_class;
};

CRYPTIC_EXPORT GType cryptic_qrg_get_type(void);

//CRYPTIC_EXPORT CrypticQRG* cryptic_qrg_new(int lg_modulus, BN_GENCB *cb);
CRYPTIC_EXPORT CrypticQRG* cryptic_qrg_new(int lg_modulus);
//CRYPTIC_EXPORT CrypticQRG* cryptic_qrg_new_load(BIGNUM *p, BIGNUM *n, BN_GENCB *cb);
CRYPTIC_EXPORT CrypticQRG* cryptic_qrg_new_load(BIGNUM *p, BIGNUM *n, BIGNUM *base);

//CRYPTIC_EXPORT int cryptic_qrg_verif_generator(CrypticQRG *qrg);
CRYPTIC_EXPORT int cryptic_qrg_check_qr(BIGNUM *qr, BIGNUM *modulus);
CRYPTIC_EXPORT int cryptic_qrg_pick_base(CrypticQRG *qrg, BIGNUM *out_base);
CRYPTIC_EXPORT int cryptic_qrg_pick_k_bases(CrypticQRG *qrg, BIGNUM **out_bases, int nb_bases);

/* Accessors */
CRYPTIC_EXPORT BIGNUM* cryptic_qrg_get_p(CrypticQRG *qrg);
CRYPTIC_EXPORT BIGNUM* cryptic_qrg_get_pp(CrypticQRG *qrg);
CRYPTIC_EXPORT BIGNUM* cryptic_qrg_get_q(CrypticQRG *qrg);
CRYPTIC_EXPORT BIGNUM* cryptic_qrg_get_qq(CrypticQRG *qrg);
CRYPTIC_EXPORT BIGNUM* cryptic_qrg_get_order(CrypticQRG *qrg);
CRYPTIC_EXPORT BIGNUM* cryptic_qrg_get_phi(CrypticQRG *qrg);
CRYPTIC_EXPORT BIGNUM* cryptic_qrg_get_n(CrypticQRG *qrg);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* CRYPTIC_MATHS_QR_GROUP_H */
