/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <openssl/bn.h>

#include "../errors.h"
#include "../utils.h"

/**
 * cryptic_find_random
 * @size: number of bits for the random..
 *
 * Find a random of size bits.
 *
 * Return value: CRYPTIC_NO_ERROR if successful; or an error code if an error
 *     occured
 **/
int
cryptic_find_random(BIGNUM *ret, int size)
{
	int rc = CRYPTIC_ERROR_UNDEFINED;

	BIGNUM *s = NULL, *two = NULL;
	BN_CTX *ctx = NULL;

	goto_cleanup_if_fail_with_rc_with_warning_openssl(s = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(two = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(two,2) == 1);

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ctx = BN_CTX_new());

	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(s,size) == 1);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_exp(s,two,s,ctx) == 1);

	do{
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_rand_range(ret,s) == 1);
	} while(BN_num_bits(ret) != size);

	rc = CRYPTIC_NO_ERROR;
cleanup:
	cryptic_release_ctx(ctx);
	cryptic_release_bn(s);
	cryptic_release_bn(two);
	return rc;
}

/**
 * cryptic_find_random_with_range_value
 * @value: reference value.
 *
 * Find a random of a same number of bits as value.
 *
 * Return value: CRYPTIC_NO_ERROR if successful; or an error code if an error
 *     occured
 **/
int
cryptic_find_random_with_range_value(BIGNUM *ret, BIGNUM *value)
{
	int rc = CRYPTIC_ERROR_UNDEFINED;

	BN_CTX *ctx = NULL;
	goto_cleanup_if_fail_with_rc_with_warning_openssl(ctx = BN_CTX_new());

	int size = BN_num_bits(value);

	do{
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_rand_range(ret,value) == 1);
	} while(BN_num_bits(ret) != size);

	rc = CRYPTIC_NO_ERROR;
cleanup:
	cryptic_release_ctx(ctx);
	return rc;
}
/**
 * cryptic_ret_random
 * @size: number of bits for the random..
 *
 * Find a random of size bits.
 *
 * Return value: CRYPTIC_NO_ERROR if successful; or an error code if an error
 *     occured
 **/

BIGNUM*
cryptic_ret_random(int size)
{
	int rc = CRYPTIC_ERROR_UNDEFINED;

	BIGNUM *s = NULL, *two = NULL, *ret = NULL;
	BN_CTX *ctx = NULL;

	goto_cleanup_if_fail_with_rc_with_warning_openssl(s = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(ret = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(two = BN_new());
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(two,2) == 1);

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ctx = BN_CTX_new());

	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_set_word(s,size) == 1);
	goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_exp(s,two,s,ctx) == 1);

	do{
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_rand_range(ret,s) == 1);
	} while(BN_num_bits(ret) != size);

	rc = CRYPTIC_NO_ERROR;
cleanup:
	cryptic_release_ctx(ctx);
	cryptic_release_bn(s);
	cryptic_release_bn(two);
	if(rc != CRYPTIC_NO_ERROR){
		cryptic_release_bn(ret);
		return NULL;
	}
	return ret;
}

/**
 * cryptic_ret_random_with_range_value
 * @value: reference value.
 *
 * Find a random of a same number of bits as value.
 *
 * Return value: CRYPTIC_NO_ERROR if successful; or an error code if an error
 *     occured
 **/
BIGNUM*
cryptic_ret_random_with_range_value(BIGNUM *value)
{
	int rc = CRYPTIC_ERROR_UNDEFINED;

	BIGNUM *ret = NULL;
	BN_CTX *ctx = NULL;

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ret = BN_new());

	goto_cleanup_if_fail_with_rc_with_warning_openssl(ctx = BN_CTX_new());

	int size = BN_num_bits(value);

	do{
		goto_cleanup_if_fail_with_rc_with_warning_openssl(BN_rand_range(ret,value) == 1);
	} while(BN_num_bits(ret) != size);

	rc = CRYPTIC_NO_ERROR;
cleanup:
	cryptic_release_ctx(ctx);
	if(rc != CRYPTIC_NO_ERROR){
		cryptic_release_bn(ret);
		return NULL;
	}
	return ret;
}
