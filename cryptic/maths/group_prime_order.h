/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_MATHS_PRIME_ORDER_GROUP_H
#define CRYPTIC_MATHS_PRIME_ORDER_GROUP_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <glib.h>
#include <glib-object.h>
#include <openssl/bn.h>

#include "../export.h"

#define CRYPTIC_TYPE_GROUPSPRIMEORDER (cryptic_prime_order_group_get_type())
#define CRYPTIC_GROUPSPRIMEORDER(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), CRYPTIC_TYPE_GROUPSPRIMEORDER, CrypticPrimeOrderGroup))
#define CRYPTIC_GROUPSPRIMEORDER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), CRYPTIC_TYPE_GROUPSPRIMEORDER, CrypticPrimeOrderGroupClass))
#define CRYPTIC_IS_GROUPSPRIMEORDER(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), CRYPTIC_TYPE_GROUPSPRIMEORDER))
#define CRYPTIC_IS_GROUPSPRIMEORDER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CRYPTIC_TYPE_GROUPSPRIMEORDER))
#define CRYPTIC_GROUPSPRIMEORDER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CRYPTIC_TYPE_GROUPSPRIMEORDER, CrypticPrimeOrderGroupClass))

typedef struct _CrypticPrimeOrderGroup CrypticPrimeOrderGroup;
typedef struct _CrypticPrimeOrderGroupClass CrypticPrimeOrderGroupClass;

/**
 * CrypticPrimeOrderGroup:
 * @p: group modulus - safe prime.
 * @pp: Cathy-germain of p.
 * @order: group order.
 * @generator: group generator.
 * @bases: table of generators used as bases for DL representation.
 * @nb_generator: size of the table bases.
 *
 * Group of prime order.
 *
 */
struct _CrypticPrimeOrderGroup{
	GObject parent;

	/*< public >*/
	BIGNUM *p;
	BIGNUM *pp;
	BIGNUM *order;
	BIGNUM *generator;
	BIGNUM **bases;
	int nb_bases;
};

struct _CrypticPrimeOrderGroupClass {
	GObjectClass parent_class;
};

CRYPTIC_EXPORT GType cryptic_prime_order_group_get_type(void);

//CRYPTIC_EXPORT CrypticPrimeOrderGroup* cryptic_prime_order_group_new(int lg_modulus, BN_GENCB *cb);
CRYPTIC_EXPORT CrypticPrimeOrderGroup* cryptic_prime_order_group_new(int lg_modulus);
CRYPTIC_EXPORT int cryptic_prime_order_group_more_bases(CrypticPrimeOrderGroup *group, int nb);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* CRYPTIC_MATHS_PRIME_ORDER_GROUP_H */
