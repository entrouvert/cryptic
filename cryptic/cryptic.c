/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2010 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <glib.h>
#include <glib-object.h>

#include "cryptic.h"
#include "errors.h"

#include "maths/decompose_integer.h"
#include "maths/quadratic_residues_group.h"
#include "maths/group_prime_order.h"
#include "protocols/pok_schnorr/schnorr_zkpk.h"
#include "protocols/pok_schnorr/hash_for_ni_proofs.h"
#include "protocols/proof_range/proof_range_in_qrg.h"
#include "protocols/clsig/clsig.h"
#include "protocols/clsig/commit_data_store.h"

/**
 * cryptic_init:
 *
 * Initializes Cryptic library.
 *
 * Return value: 0 on success; or a negative value otherwise.
 **/
int cryptic_init()
{

	g_type_init();

	/* Init Cryptic classes */
	cryptic_decompose_integer_get_type();
	cryptic_qrg_get_type();
	cryptic_prime_order_group_get_type();
	cryptic_zkpk_schnorr_get_type();
	cryptic_proofrange_qrg_get_type();
	cryptic_commit_data_store_get_type();
	cryptic_clsig_get_type();
	cryptic_hash_for_ni_proofs_get_type();

	return(CRYPTIC_NO_ERROR);
}

