/* $Id: export.h 3990 2008-09-12 15:06:58Z bdauvergne $
 *
 * Cryptic -- Cryptographic tools and protocols
 * 
 * Copyright (C) 2010 Entr'ouvert
 *
 * Authors: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __CRYPTIC_EXPORT_H__
#define __CRYPTIC_EXPORT_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Now, the export orgy begins. The following we must do for the
 * Windows platform with MSVC compiler. */

#if !defined CRYPTIC_EXPORT
#  if (defined _MSC_VER || defined MINGW32)
	/* if we compile libxmlsec itself: */
#    if defined(IN_CRYPTIC)
#      if !defined(CRYPTIC_STATIC)
#        define CRYPTIC_EXPORT __declspec(dllexport)
#      else
#        define CRYPTIC_EXPORT extern
#      endif
	/* if a client program includes this file: */
#    else
#      if !defined(CRYPTIC_STATIC)
#        define CRYPTIC_EXPORT __declspec(dllimport)
#      else
#        define CRYPTIC_EXPORT
#      endif
#    endif
	/* This holds on all other platforms/compilers, which are easier to
	   handle in regard to this. */
#  else
#    define CRYPTIC_EXPORT
#  endif
#endif

#if !defined CRYPTIC_EXPORT_VAR
#  if (defined _MSC_VER || defined MINGW32)
	/* if we compile libxmlsec itself: */
#    if defined(IN_CRYPTIC)
#      if !defined(CRYPTIC_STATIC)
#        define CRYPTIC_EXPORT_VAR __declspec(dllexport) extern
#      else
#        define CRYPTIC_EXPORT_VAR extern
#      endif
	/* if we compile libxmlsec-crypto itself: */
#    elif defined(IN_CRYPTIC_CRYPTO)
#        define CRYPTIC_EXPORT_VAR extern
	/* if a client program includes this file: */
#    else
#      if !defined(CRYPTIC_STATIC)
#        define CRYPTIC_EXPORT_VAR __declspec(dllimport) extern
#      else
#        define CRYPTIC_EXPORT_VAR extern
#      endif
#    endif
	/* This holds on all other platforms/compilers, which are easier to
	   handle in regard to this. */
#  else
#    define CRYPTIC_EXPORT_VAR extern
#  endif
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CRYPTIC_EXPORT_H__ */
