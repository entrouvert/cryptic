/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdio.h>

#include <openssl/bn.h>
#include <openssl/err.h>
#include <openssl/bio.h>

//#include "../cryptic.h"
#include "../protocols/clsig/clsig.h"
#include "../errors.h"
#include "print.h"

int
cryptic_print_bn(char *prefix, BIGNUM *bn)
{
	char *hex = NULL;
	if(bn){
		hex = BN_bn2hex(bn);
	}
	printf("%s%s\n", prefix, hex);
	OPENSSL_free(hex);
	return CRYPTIC_NO_ERROR;
}

int
cryptic_print_private_CLSIG_parameters(CrypticClsig *clsig)
{
	printf("SIGNER PRIVATE DATA\n");
	if(clsig->qrg){
		if (cryptic_qrg_get_q(clsig->qrg)) 		cryptic_print_bn("----> \tq\t",cryptic_qrg_get_q(clsig->qrg));
		if (cryptic_qrg_get_qq(clsig->qrg)) 		cryptic_print_bn("----> \tqq\t",cryptic_qrg_get_qq(clsig->qrg));
		if (cryptic_qrg_get_p(clsig->qrg)) 		cryptic_print_bn("----> \tp\t",cryptic_qrg_get_p(clsig->qrg));
		if (cryptic_qrg_get_pp(clsig->qrg)) 		cryptic_print_bn("----> \tpp\t",cryptic_qrg_get_pp(clsig->qrg));
		if (cryptic_qrg_get_order(clsig->qrg)) 	cryptic_print_bn("----> \torder\t",cryptic_qrg_get_order(clsig->qrg));
		if (cryptic_qrg_get_phi(clsig->qrg)) 	cryptic_print_bn("----> \tphi\t",cryptic_qrg_get_phi(clsig->qrg));
	}
	if (clsig->d) cryptic_print_bn("----> \td\t",clsig->d);

	return CRYPTIC_NO_ERROR;
}

int
cryptic_print_public_CLSIG_parameters(CrypticClsig *clsig)
{

	printf("SIGNER PUBLIC DATA\n");
	if (clsig->modulus) cryptic_print_bn("----> \tn\t",clsig->modulus);
	if (clsig->S) cryptic_print_bn("----> \tS\t",clsig->S);
	if (clsig->Z) cryptic_print_bn("----> \tZ\t",clsig->Z);
	int i;
	for(i=0;i<clsig->nb_bases;i++){
		if (clsig->bases[i]){
			printf("----> \tR%d",i);
			cryptic_print_bn("\t",clsig->bases[i]);
		}
	}
	if (clsig->e) cryptic_print_bn("----> \te\t",clsig->e);

	return CRYPTIC_NO_ERROR;
}

int
cryptic_print_CLSIG_lengths(CrypticClsig *clsig)
{
	printf("Bit lengths of the clsig system:\n");
	printf("lg_modulus:\t\t%d\n",clsig->lg_modulus);
	printf("lg_exponent:\t\t%d\n",clsig->lg_exponent);
	printf("interval_exponent:\t%d\n",clsig->interval_exponent);
	printf("lg_blind:\t\t%d\n",clsig->lg_blind);
	printf("lg_randomize:\t\t%d\n",clsig->lg_randomize);
	printf("lg_sec_param:\t\t%d\n",clsig->lg_sec_param);
	printf("lg_zk_sec_param:\t%d\n",clsig->lg_zk_sec_param);
	printf("lg_clsig_sec_param:\t%d\n",clsig->lg_clsig_sec_param);
	printf("nb_bases:\t\t%d\n",clsig->nb_bases);

	return CRYPTIC_NO_ERROR;
}
