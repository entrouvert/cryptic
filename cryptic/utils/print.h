/* Cryptic -- Cryptographic tools and protocols
 * Copyright (C) 2009 Mikaël Ates <mates@entrouvert.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CRYPTIC_UTILS_PRINT_H
#define CRYPTIC_UTILS_PRINT_H

#include <openssl/bn.h>
#include "../protocols/clsig/clsig.h"

#include "../export.h"

CRYPTIC_EXPORT int cryptic_print_bn(char *prefix, BIGNUM *bn);
int cryptic_print_private_CLSIG_parameters(CrypticClsig* clsig);
int cryptic_print_public_CLSIG_parameters(CrypticClsig* clsig);
int cryptic_print_CLSIG_lengths(CrypticClsig* clsig);

#endif
